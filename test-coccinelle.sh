#!/bin/sh
# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

VALID_ARGS="--kernel_dir --kernel_config --arch --cross_compile --smatch"

CI_DIR=$(dirname "$(realpath "$0")")
. "$CI_DIR"/lib/common-init.sh

if [ -r "$KERNEL_CONFIG" ]; then
  KERNEL_CONFIG=$(realpath "$KERNEL_CONFIG")
fi

cd "$KERNEL_DIR"

# Remove non mandatory style
rm -f ./scripts/coccinelle/api/string_choices.cocci

# TODO Remove failing script.
rm -f ./scripts/coccinelle/misc/secs_to_jiffies.cocci

if ! which spatch >/dev/null ; then
  echo "Unable to find the spatch binary"
  exit 2
fi

rm -f build.log build.err
for dir in drivers/media/ drivers/staging/media/ drivers/input/touchscreen/; do
  COMBINED_LOG="dir.log" ERR_LOG="dir.err" tee_log make coccicheck M="$dir" MODE=report
  cat dir.log >> build.log
  cat dir.err >> build.err
done

sh "$CI_DIR"/testdata/static/parse-coccinelle_log.sh build.log

. "$CI_DIR"/lib/common-end.sh
