/*
 * Copyright 2024 Hans Verkuil <hverkuil@xs4all.nl>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//#include "../uapi/linux/v4l2-controls.h"

#include <linux/videodev2.h>

struct v4l2_area area;

struct v4l2_ctrl_h264_sps h264_sps;
struct v4l2_ctrl_h264_pps h264_pps;
struct v4l2_ctrl_h264_scaling_matrix h264_scaling_matrix;
struct v4l2_h264_weight_factors h264_weight_factors;
struct v4l2_ctrl_h264_pred_weights h264_weight_table;
struct v4l2_h264_reference h264_reference;
struct v4l2_ctrl_h264_slice_params h264_slice_params;
struct v4l2_ctrl_h264_decode_params h264_decode_params;

struct v4l2_ctrl_fwht_params fwht;

struct v4l2_ctrl_mpeg2_sequence mpeg2_seq;
struct v4l2_ctrl_mpeg2_picture mpeg2_picture;
struct v4l2_ctrl_mpeg2_quantisation mpeg2_quant;

struct v4l2_ctrl_hevc_sps hevc_sps;
struct v4l2_ctrl_hevc_pps hevc_pps;
struct v4l2_hevc_dpb_entry hevc_entry;
struct v4l2_hevc_pred_weight_table hevc_weight_table;
struct v4l2_ctrl_hevc_slice_params hevc_params;

struct v4l2_vp8_segment vp8_seg;
struct v4l2_vp8_loop_filter vp8_loop_filter;
struct v4l2_vp8_quantization vp8_quant;
struct v4l2_vp8_entropy vp8_entropy;
struct v4l2_vp8_entropy_coder_state vp8_entropy_coder_state;
struct v4l2_ctrl_vp8_frame vp8_frame;

struct v4l2_ctrl_av1_sequence av1_sequence;
struct v4l2_ctrl_av1_tile_group_entry av1_tile_group_entry;
struct v4l2_ctrl_av1_frame av1_frame;
struct v4l2_ctrl_av1_film_grain av1_film_grain;
