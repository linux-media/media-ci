#!/bin/sh
# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if [ -z "$1" ]; then
  logfile="-"
else
  logfile="$1"
fi

grep -i -E "(error:|warning:|warn:|Error 1)" "$logfile" | grep -v "Error 1 (ignored)" |
   grep -v "Error 139" |
   grep -v "turning off implications after" |
   grep -v -E "(cx23885-dvb.c|cx88-dvb.c|saa7134-dvb.c|mb86a16.c|solo6x10|mm.h|page-flags.h).*too hairy" >&2 && exit 1

exit 0
