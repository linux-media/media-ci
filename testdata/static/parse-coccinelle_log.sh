#!/bin/sh
# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if [ -z "$1" ]; then
  logfile="-"
else
  logfile="$1"
fi

grep -i -E "(error:|warning:)" "$logfile" |
  grep -v "be a metavariable?$" |
  grep -v -E "(goodix.c|edt-ft5x06.c|iqs7211.c):" | # Ignore non media files
  grep -v -E "npcm-video.c:.*missing put_device" |
  grep -v -E "dvb_frontend.c.*preceding lock on line" |
  grep -v -E "(vdec.c|venc.c|tda10048.c).*WARNING: do_div" >&2 && exit 1

exit 0
