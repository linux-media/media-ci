#!/usr/bin/python3
# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import os
import re
import sys
import logging
import requests
from shutil import which
from urllib.parse import quote_plus
from smtplib import SMTP
from email.message import EmailMessage
import email.utils
import subprocess
from config import *
from media_paths import is_media_patch
import dateutil.parser
import datetime
from functools import cache
from junitparser import JUnitXml

logger = logging.getLogger(__name__)

EMAIL_WAIT_SECS = 60 * 10  # 10 minutes


def cover_or_patch(series, name):
    if series["cover_letter"] != None:
        return series["cover_letter"][name]
    return series["patches"][0][name]


def msgid_from_series(series):
    msgid = cover_or_patch(series, "msgid")
    if msgid[0] == "<":
        msgid = msgid[1:]
    if msgid[-1] == ">":
        msgid = msgid[:-1]
    return msgid


def series_maybe_pr(series):
    if not series["name"]:
        return False
    return "PULL" in series["name"]


def branch_from_series(series):
    return f"series_{series['id']}"


def send_email(series, content):
    msg = EmailMessage()
    msg["Subject"] = f"Re: {cover_or_patch(series,'name')}"
    msg["From"] = "Patchwork Integration <patchwork@media-ci.org>"
    msg["To"] = email.utils.formataddr(
        (series["submitter"]["name"], series["submitter"]["email"])
    )
    msg["Reply-To"] = "linux-media@vger.kernel.org"
    msg["Bcc"] = "patchwork@linuxtv.org"
    msg["In-Reply-To"] = cover_or_patch(series, "msgid")
    msg["References"] = msg["In-Reply-To"]
    msg.set_content(content)

    if series_maybe_pr(series):
        msg["Cc"] = "linuxtv-commits@linuxtv.org"

    server = SMTP(SMTP_SERVER, SMTP_PORT)
    server.starttls()
    server.login(SMTP_USER, SMTP_PASSWORD)
    server.send_message(msg)


EMAIL_BYE = """
Best regards, and Happy Hacking!
Media CI robot on behalf of the linux-media community.

---
Check the latest rules for contributing your patches at:
https://docs.kernel.org/driver-api/media/maintainer-entry-profile.html

If you believe that the CI is wrong, kindly open an issue at
https://gitlab.freedesktop.org/linux-media/media-ci/-/issues or reply-all
to this message.
"""


def send_email_ok(series, html_report):
    content = f"""Dear {series["submitter"]["name"]}:

Thanks for your patches!

The Media CI robot had a great time going through them, and it could
not find any issue.

For more details, check the full report at:
{html_report} .

{EMAIL_BYE}
"""
    return send_email(series, content)


def send_email_error(series, html_report, boring_report):
    log = requests.get(boring_report).text
    content = f"""Dear {series["submitter"]["name"]}:

Thanks for your patches! Unfortunately the Media CI robot detected some
issues:

{log}

Please fix your series, and upload a new version. If you have a patchwork
account, do not forget to mark the current series as Superseded.

For more details, check the full report at:
{html_report} .


{EMAIL_BYE}
"""
    return send_email(series, content)


def send_email_unable_to_run_ci(series, stderr):
    msgid = msgid_from_series(series)
    if stderr:
        error_msg = f"\nError message:\{stderr}\n"
    else:
        error_msg = ""

    content = f"""Dear {series["submitter"]["name"]}:

Thanks for your patches! Unfortunately the Media CI robot has not been
able to test them.

Make sure that the whole series {msgid} is
available at lore. And that it can be cherry-picked on top the "next"
branch of "{COMMITTERS_URI}".

You can try something like this:
    git fetch {COMMITTERS_URI} next
    git checkout FETCH_HEAD
    b4 shazam {msgid}
{error_msg}
{EMAIL_BYE}
"""
    return send_email(series, content)


def find_patch(patches, patch_number):
    if not patches:
        return None
    if patch_number == -1:
        return patches[-1]

    for p in patches:
        res = re.search(r"^\[.*?(\d+)/\d+]", p["name"])
        if not res:
            if patch_number == 1:
                return p
            continue
        if patch_number == int(res.group(1)):
            return p

    return None


def patchwork_set_check(
    series,
    state,
    description,
    target_url=f"https://{GITLAB_SERVER}/{GITLAB_PROJECT}",
    context="media-ci",
    patch_number=-1,
):

    # Patchwork is not particularly happy if we send tons of checks. Implement a retry
    s = requests.Session()
    retries = requests.packages.urllib3.util.retry.Retry(
        total=3,
        backoff_factor=10,
    )
    s.mount("https://", requests.adapters.HTTPAdapter(max_retries=retries))

    context = re.sub("[^0-9a-zA-Z\_\-]+", "_", context)

    check_state = {
        "state": state,
        "target_url": target_url,
        "context": context,
        "description": description,
    }

    patch = find_patch(series["patches"], patch_number)
    if not patch:
        logger.warning(f"Invalid patch number {patch_number} {len(series['patches'])}")
        return

    r_checks = s.post(
        f"{PATCHWORK_API}/patches/{patch['id']}/checks/",
        json=check_state,
        headers={"Authorization": f"Token {PATCHWORK_TOKEN}"},
        verify=PW_VERIFY,
    )
    if r_checks.status_code != 201:
        raise Exception(f"status_code: {r_checks.status_code}, txt:{r_checks.text}")


def link_from_testcase(case):
    for txt in (case.system_err, case.system_out):
        if not txt:
            continue
        res = re.search(r"^\[\[ATTACHMENT\|(.*?)\]\]", txt)
        if res:
            return res.group(1)

    return None


def set_patchwork_check_individual_patch(series, job_id, job_url):
    junit_uri = f"{PAGES_URL}/-/jobs/{job_id}/artifacts/junit/junit.xml"
    junit = requests.get(junit_uri)
    if junit.status_code != 200:
        return
    xml = JUnitXml.fromstring(junit.content)
    for suite in xml:
        for case in suite:
            # Is an individual patch check?
            res = re.search(r"(\d{4})-", case.name)
            if not res:
                continue
            patch_number = int(res.group(1))
            if case.result:
                result = "fail"
            else:
                result = "success"
            url = link_from_testcase(case)
            if url:
                job_url = (
                    f'{PAGES_URL}/-/jobs/{job_id}/artifacts/{url.replace(" ", "%20")}'
                )
            res = re.search(r" (.*)", case.name)
            if res:
                name = res.group(1)
            else:
                name = suite.name
            patchwork_set_check(
                series,
                result,
                "Link",
                job_url,
                context=f"{patch_number}-{name}",
                patch_number=patch_number,
            )


def set_patchwork_check_from_gitlab_pipeline(series):
    logger.info(f"Download Pipeline state from Gitlab")
    # Fetch pipeline from gitlab and check if its completed
    branch = branch_from_series(series)
    r_pipeline = requests.get(
        f"https://{GITLAB_SERVER}/api/v4/projects/{quote_plus(GITLAB_PROJECT)}/pipelines/latest?ref={quote_plus(branch)}",
    ).json()

    pipeline_status = None
    if r_pipeline:
        pipeline_status = r_pipeline.pop("status", None)

    if pipeline_status not in ("success", "failed"):
        logger.info(f"Pipeline not ready yet: {pipeline_status}")
        return

    # Parse the pipeline jobs
    logger.info(f"Download Jobs state from Gitlab. Pipeline {r_pipeline['id']}")
    r_jobs = requests.get(
        f"https://{GITLAB_SERVER}/api/v4/projects/{quote_plus(GITLAB_PROJECT)}/pipelines/{r_pipeline['id']}/jobs"
    ).json()

    # Send email
    error_found = False
    for job in r_jobs:
        if job["name"] == "report" and job["status"] == "success":
            html_report = f"{PAGES_URL}/-/jobs/{job['id']}/artifacts/report.htm"
            boring_report = (
                f"{PAGES_URL}/-/jobs/{job['id']}/artifacts/report_boring.txt"
            )
        if job["status"] != "success":
            error_found = True
    if error_found:
        logger.info(f"Sending email")
        send_email_error(series, html_report, boring_report)
    elif series_maybe_pr(series):
        logger.info(f"Sending email")
        send_email_ok(series, html_report)

    # Send Patchwork Checks
    logger.info(f"Saving checks")
    if pipeline_status == "success":
        err_status = "warning"
    else:
        err_status = "fail"
    patchwork_set_check(series, "success", "Link", html_report, context="HTML_report")
    for job in r_jobs:
        if job["status"] == "success":
            status = "success"
        else:
            status = err_status
        url = f"https://{GITLAB_SERVER}/{GITLAB_PROJECT}/-/jobs/{job['id']}"
        logger.info(f"Save job {job['name']}")
        patchwork_set_check(series, status, "Link", url, context=job["name"])
        if not series_maybe_pr(series):
            set_patchwork_check_individual_patch(series, job["id"], url)


@cache
def fetch_branch(branch):
    logger.debug(f"Fetch committers/{branch} branch")
    subprocess.run(
        ["git", "fetch", COMMITTERS_URI, branch], check=True, capture_output=True
    )
    git = subprocess.run(
        ["git", "rev-parse", "FETCH_HEAD"],
        check=True,
        capture_output=True,
        text=True,
    )
    return git.stdout.strip()


def get_user_branch(series, all_branches):
    if not series["name"]:
        return None
    for conf in re.findall(r"\[.*?\]", series["name"]):
        for b in all_branches:
            if b == "fixes":  # Some people name fixes as fix
                b = "fix"
            if conf.lower().find(b) != -1:
                return b
    return None


def get_b4_branch(mbox, all_branches):
    txt = open(mbox, "rb").read().decode(errors="replace")
    m = re.search("\nbase-commit: ([\da-f]*)", txt)
    if not m:
        return None
    base_commit = m.group(1)

    for branch in all_branches:
        sha = fetch_branch(branch)
        git = subprocess.run(
            ["git", "merge-base", "--is-ancestor", base_commit, sha],
            capture_output=True,
        )
        if git.returncode == 0:
            return branch, sha

    return None


# Inspired in Mauro's git-basetag.py
def find_sha_from_index(filename, sha, index):
    git = subprocess.run(
        f"git log --format=format:%H {sha} -- {filename}".split(),
        capture_output=True,
        text=True,
    )
    if git.returncode:
        return None
    tags = map(lambda x: x.strip(), git.stdout.splitlines())
    for sha in tags:
        blob = subprocess.run(
            f"git ls-tree {sha} -- {filename}".split(), capture_output=True, text=True
        )
        if blob.returncode or len(blob.stdout.split()) < 3:
            continue
        if blob.stdout.split()[2].startswith(index):
            return sha
    return None


def get_patch_parent(mbox, base_branch):
    logger.info(f"Trying to find parent of series, It can take a while...")
    branch_sha = fetch_branch(base_branch)
    txt = open(mbox, "rb").read().decode(errors="replace")

    files = re.findall(
        r"\ndiff --git a/(.*?) b/.*?\nindex ([\da-f]+)\.\.[\da-f]+ [\d]+\n", txt
    )
    shas = set()
    done_files = set()
    for file, index in files:
        if file in done_files:
            continue
        done_files.add(file)
        sha = find_sha_from_index(file, branch_sha, index)
        if not sha:
            continue
        shas.add(sha)

    git = subprocess.run(
        f"git rev-list --topo-order --no-walk".split() + list(shas),
        capture_output=True,
        text=True,
    )
    if git.returncode:
        logger.info(f"Unable to find parent")
        return None
    return git.stdout.splitlines()[0].strip()


def testeable_heads(series, mbox, is_pr):
    all_branches = ["next", "fixes"]

    # If the user specifies a branch, use it
    user_branch = get_user_branch(series, all_branches)
    if user_branch:
        yield user_branch, fetch_branch(user_branch)
        return

    # Use b4 to get a hint
    b4_branch = get_b4_branch(mbox, all_branches)
    if b4_branch:
        yield b4_branch
        all_branches.remove(b4_branch[0])

    for branch in all_branches:
        yield branch, fetch_branch(branch)

    if is_pr:
        return

    # Guess the parent as last resource
    sha = get_patch_parent(mbox, "next")
    if sha:
        yield None, sha

    return


def reset_repo(mbox=None):
    subprocess.run(["git", "rebase", "--abort"], capture_output=True)
    subprocess.run(["git", "am", "--abort"], capture_output=True)
    subprocess.run(["git", "reset", "--hard"], capture_output=True)
    if mbox:
        subprocess.run(["git", "clean", "-e", mbox, "-fxd"], capture_output=True)


def try_apply_series(sha, mbox, base_branch):
    logger.info(f"Trying to apply series on {sha} ({base_branch})")
    reset_repo(mbox)
    subprocess.run(["git", "checkout", sha], check=True, capture_output=True)
    b4 = subprocess.run(
        ["b4", "--offline-mode", "shazam", "-l", "-m", mbox],
        capture_output=True,
        text=True,
    )
    if b4.returncode:
        return b4.stderr
    return None


def try_pull_request(sha, pr_head, base_branch):
    logger.info(f"Rebasing PR head {pr_head} on {sha} ({base_branch})")
    reset_repo()
    subprocess.run(["git", "checkout", pr_head], check=True, capture_output=True)
    git = subprocess.run(
        ["git", "rebase", sha],
        capture_output=True,
        text=True,
    )
    if git.returncode:
        return git.stderr
    return None


def gitlab_trigger_ci(series):
    # Check if branch already exists
    dest_branch = branch_from_series(series)
    CI_URI = (
        f"https://gitlab-ci-token:{GITLAB_TOKEN}@{GITLAB_SERVER}/{GITLAB_PROJECT}.git"
    )
    ls_remote = subprocess.run(
        ["git", "ls-remote", "--exit-code", CI_URI, f"refs/heads/{dest_branch}"],
        capture_output=True,
    )
    if ls_remote.returncode != 2:
        logger.info(f"Branch already created in Gitlab")
        return

    logger.info(f"Fetch series")
    mbox = "series.mbx"
    msgid = msgid_from_series(series)
    b4 = subprocess.run(
        ["b4", "mbox", "-n", mbox, msgid], capture_output=True, text=True
    )
    if b4.returncode:
        # Only consider error if lore had enough time to process the series.
        timediff = datetime.datetime.now() - dateutil.parser.parse(series["date"])
        if timediff.seconds < EMAIL_WAIT_SECS:
            logger.info(f"Too early to consider a b4 mbox error")
            return
        patchwork_set_check(series, "fail", "Failed to fetch patch")
        send_email_unable_to_run_ci(series, b4.stderr)
        return

    is_pr = False
    b4 = subprocess.run(["b4", "--no-stdin", "pr", msgid], capture_output=True)
    if not b4.returncode:
        logger.info(f"Series is a PR")
        is_pr = True
        git = subprocess.run(
            ["git", "rev-parse", "FETCH_HEAD"],
            check=True,
            capture_output=True,
            text=True,
        )
        pr_head = git.stdout.strip()

    out_msg = ""
    for base_branch, base_sha in testeable_heads(series, mbox, is_pr):
        # Cherry-pick series
        if is_pr:
            ret = try_pull_request(base_sha, pr_head, base_branch)
        else:
            ret = try_apply_series(base_sha, mbox, base_branch)
        if not ret:
            break
        out_msg += f"\nTrying branch {base_branch} {base_sha}...\n"
        out_msg += ret
    else:
        patchwork_set_check(series, "fail", "Failed to cherry-pick patch series")
        send_email_unable_to_run_ci(series, out_msg)
        return

    git = subprocess.run(
        ["git", "rev-parse", "HEAD"], check=True, capture_output=True, text=True
    )
    if git.stdout.strip() == base_sha:
        patchwork_set_check(series, "warning", "Patchset does not contain any change")
        send_email_unable_to_run_ci(series, None)
        return

    diff = subprocess.run(
        ["git", "diff", f"{base_sha}..HEAD"],
        check=True,
        capture_output=True,
        text=True,
    )
    if not is_media_patch(diff.stdout):
        patchwork_set_check(
            series, "warning", "Patchset does not modify media files. Ignoring"
        )
        return

    logger.info(f"Push {dest_branch} to CI")
    command = [
        "git",
        "push",
        "-f",
        "-o",
        f"ci.variable=GIT_ORIGIN={base_sha}",
        CI_URI,
        f"HEAD:refs/heads/{dest_branch}",
    ]
    subprocess.run(command, check=True)


def patchwork_process_all_series():
    r_all_series = requests.get(
        f"{PATCHWORK_API}/series/?order=-id",
        verify=PW_VERIFY,
    ).json()
    for series in r_all_series:
        logger.info(f'Processing series {series["id"]}: {series["name"]}')

        if len(series["patches"]) == 0:
            logger.info(f"Series without patches. Continuing")
            continue

        if not series["received_all"]:
            timediff = datetime.datetime.now() - dateutil.parser.parse(series["date"])
            if timediff.seconds < EMAIL_WAIT_SECS:
                logger.info(f"Series not completed, continuing")
                continue
            logger.info(f"Series not completed, but old enough... Processing it")

        if not series["patches"][-1]["list_archive_url"]:
            logger.info(f"List archive URL not found, continuing")
            continue

        # Inspect the check of the last patch
        id_last = series["patches"][-1]["id"]
        r_checks = requests.get(
            f"{PATCHWORK_API}/patches/{id_last}/checks", verify=PW_VERIFY
        ).json()

        # This series has been tested
        if r_checks:
            logger.info(f"Series already processed. Continuing")
            continue

        # Trigger CI if needed
        gitlab_trigger_ci(series)

        # Get status from gitlab
        set_patchwork_check_from_gitlab_pipeline(series)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    if not os.path.isdir(".git"):
        logger.error(
            "This command must be executed from a non-bare git repository. Exiting!"
        )
        sys.exit(-1)

    for cmd in "b4", "git":
        if not which(cmd):
            logger.error(f"{cmd}: not found in path. Exiting!")
            sys.exit(-1)

    patchwork_process_all_series()
