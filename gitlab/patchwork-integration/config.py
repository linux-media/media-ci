#!/usr/bin/python3
# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import subprocess


def get_secret(name):
    ret = subprocess.run(
        ["gcloud", "secrets", "versions", "access", "latest", "--secret", name],
        check=True,
        capture_output=True,
    )
    return ret.stdout.decode("utf-8")


PATCHWORK_API = "https://patchwork.linuxtv.org/api/1.3"
PATCHWORK_TOKEN = get_secret("patchwork_token")
PW_VERIFY = True
COMMITTERS_URI = "https://gitlab.freedesktop.org/linux-media/media-committers.git"
GITLAB_SERVER = "gitlab.freedesktop.org"
GITLAB_PROJECT = "linux-media/users/patchwork"
GITLAB_TOKEN = get_secret("ci_token")
PAGES_URL = "https://linux-media.pages.freedesktop.org/-/users/patchwork"
SMTP_SERVER = "smtp.gmail.com"
SMTP_PORT = 587
SMTP_USER = "media-ci@ribalda.com"
SMTP_PASSWORD = get_secret("smtp_password")
