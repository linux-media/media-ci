# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

.templates_sha: &template_sha 9568e38927f9e9c48d4f586f84a071c3a4bdcd39 # see https://docs.gitlab.com/ee/ci/yaml/#includefile
include:
  - project: 'freedesktop/ci-templates'
    ref: *template_sha
    file: '/templates/debian.yml'

stages:
  - prep
  - lint
  - docker
  - integration
  - deploy

# Do not trigger CI for unchanged branches
workflow:
  rules:
  - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    when: always
  - if: $CI_COMMIT_BRANCH
    changes:
      paths:
      - '*'
      - '**/*'
      compare_to: refs/heads/main
    when: always

variables:
  DOCKER_TLS_CERTDIR: ""
  GET_SOURCES_ATTEMPTS: 3

.ci-run-policy:
  # Retry jobs after runner system failures
  retry:
    max: 2
    when:
      - runner_system_failure
  # Cancel CI run if a newer commit is pushed to the same branch
  interruptible: true

.ci-image-vars:
  variables:
    FDO_UPSTREAM_REPO: linux-media/media-ci
    FDO_DISTRIBUTION_VERSION: trixie-slim
    FDO_DISTRIBUTION_TAG: '2024-04-12.1'

prep:
  stage: prep
  extends:
    - .ci-run-policy
    - .ci-image-vars
    - .fdo.container-build@debian
  variables:
    FDO_DISTRIBUTION_PACKAGES: 'black shellcheck'

lint:
  stage: lint
  extends:
    - .ci-run-policy
    - .ci-image-vars
    - .fdo.distribution-image@debian
  script:
    - sh ci/shellcheck.sh
    - sh ci/black.sh
  except:
    - main
    - schedules

.docker:
  extends: .ci-run-policy
  #https://gitlab.com/gitlab-com/gl-infra/production/-/issues/17283
  image: docker:24.0.6
  services:
    - docker:24.0.6-dind
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

build_docker:
  extends: .docker
  stage: docker
  script:
    - sh docker/build.sh $DOCKER_CACHE
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: on_success
      variables:
        DOCKER_CACHE: --no_cache
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      when: never
    - when: on_success

integration:
  stage: integration
  inherit:
    variables: false
  variables:
    CI_VERSION: $CI_COMMIT_SHA
    FULL_CI: "1"
    RUN_CI_DEFAULT_BRANCH: "1"
    TEST_MEDIA_TARGET: "mc"
    GIT_ORIGIN: "HEAD~1"
  trigger:
    project: 'linux-media/users/ci'
    strategy: depend
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: on_success
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      when: never
    - when: on_success

release_docker:
  extends: .docker
  stage: deploy
  script:
    - sh docker/build.sh --release
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: on_success
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      when: always
    - when: never
