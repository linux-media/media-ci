#!/usr/bin/python3
# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import urllib.request
import gitlab
import os
import re
import sys
from junitparser import JUnitXml
import markdown


def download_file(server, project_id, job_id, file_path):
    f = urllib.request.urlopen(
        f"{server}/api/v4/projects/{project_id}/jobs/{job_id}/artifacts/{file_path}"
    )
    return f.read().decode("utf-8")


def parse_file(in_txt):
    if not in_txt:
        return None
    in_txt = in_txt.splitlines()
    m = re.search(r"\[\[ATTACHMENT\|([^]]*)\]\]", in_txt[0])
    if not m:
        return None
    txt = "\n".join(in_txt[1:])
    return {"filename": m.group(1), "text": txt}


def get_tests(server, project_id, job_id):
    try:
        junit_txt = download_file(server, project_id, job_id, "junit/junit.xml")
    except:
        return {}
    xml = JUnitXml.fromstring(str.encode(junit_txt))
    out_tests = {}
    for suite in xml:
        for case in suite:
            out_case = {}
            f = parse_file(case.system_err)
            if f:
                out_case["stderr"] = f
            f = parse_file(case.system_out)
            if f:
                out_case["stdout"] = f
            if case.result:
                out_case["status"] = "failure"
            else:
                out_case["status"] = "success"
            out_tests[case.name] = out_case
    return out_tests


def get_urls(web_url):
    m = re.search(r"https://gitlab.freedesktop.org/([^/]+)/(.*)/-/", web_url)

    gitlab_url = f"https://gitlab.freedesktop.org/{m.group(1)}/{m.group(2)}"
    pages_url = f"https://{m.group(1)}.pages.freedesktop.org/-/{m.group(2)}"
    return gitlab_url, pages_url


def process_pipeline(server, project_id, pipeline_id):
    gl = gitlab.Gitlab(server)
    project = gl.projects.get(project_id)
    pipeline = project.pipelines.get(pipeline_id)

    gitlab_url, pages_url = get_urls(pipeline.web_url)
    out = {
        "status": pipeline.status,
        "pipeline_id": pipeline_id,
        "gitlab_url": gitlab_url,
        "pages_url": pages_url,
        "trigger_user": {
            "username": pipeline.user["username"],
            "avatar": pipeline.user["avatar_url"],
            "url": pipeline.user["web_url"],
        },
    }

    out_jobs = {}
    pipeline_jobs = pipeline.jobs.list()
    for j in pipeline_jobs:
        if "commit" not in out:
            out["commit"] = {
                "author_name": j.commit["author_name"],
                "author_email": j.commit["author_email"],
                "message": j.commit["message"],
                "sha": j.commit["id"],
            }

        out_job = {
            "job_id": j.id,
            "status": j.status,
            "tests": get_tests(server, project_id, j.id),
        }

        out_jobs[j.name] = out_job
        # pprint.pprint(vars(j))
    out["jobs"] = out_jobs

    return out


def trigger_user(user):
    # return f"![{user['username']}]({user['avatar']})[{user['username']}]({user['url']})"
    return f"[{user['username']}]({user['url']})"


def print_test(pipeline, job, test, print_stdout=True):
    out = ""
    if "stdout" in test and print_stdout:
        txt = test["stdout"]["text"]
        link = f"{pipeline['pages_url']}/-/jobs/{job['job_id']}/artifacts/{test['stdout']['filename']}"
        out += f" - **[combined_log]({link}):**\n```\n{txt}\n```\n"

    if "stderr" in test:
        txt = test["stderr"]["text"]
        link = f"{pipeline['pages_url']}/-/jobs/{job['job_id']}/artifacts/{test['stderr']['filename']}"
        out += f" - **[stderr]({link}):**\n```\n{txt}\n```\n"
    return out


def print_broken_tests(pipeline):
    out = ""

    for job_name, job in pipeline["jobs"].items():
        for test_name, test in job["tests"].items():
            if test["status"] != "success":
                out += f"## Test [{job_name}:{test_name}]({pipeline['gitlab_url']}/-/jobs/{job['job_id']})\n"
                out += print_test(pipeline, job, test, print_stdout=False)

    if not out:
        return out

    return "# Broken Tests\n" + out


def generate_boring(pipeline):
    out = ""

    for job_name, job in pipeline["jobs"].items():
        for test_name, test in job["tests"].items():
            if test["status"] != "success":
                out += f"# Test {job_name}:{test_name}\n"
                if "stderr" in test:
                    out += f"{test['stderr']['text']}\n\n"

    return out


def generate_report(pipeline):
    out = f""" 
# Media-CI report

[Gitlab Link]({pipeline["gitlab_url"]}/-/pipelines/{pipeline["pipeline_id"]})

 - **Job triggered by:** {trigger_user(pipeline["trigger_user"])}

Branch Head:

 - **Commit Author:** {pipeline["commit"]["author_name"]} <{pipeline["commit"]["author_email"]}>
 - **Commit:** [{pipeline["commit"]["sha"]}]({pipeline["gitlab_url"]}/-/commit/{pipeline["commit"]["sha"]})
 - **Subject:** {pipeline["commit"]["message"].splitlines()[0]}

{print_broken_tests(pipeline)}

[TOC]
"""
    for job_name, job in pipeline["jobs"].items():
        out += f"""
## Job {job_name}

[Gitlab Link]({pipeline["gitlab_url"]}/-/jobs/{job['job_id']})

 - **Status:** {job["status"]}
"""
        for test_name, test in job["tests"].items():
            out += f"""
### Test {test_name}
 - **Status:** {test["status"]}
"""
            out += print_test(pipeline, job, test)

    return out


def send_note(pipeline, server, private_token, project_id, job_id, branch, report):
    gl = gitlab.Gitlab(server, private_token=private_token)
    project = gl.projects.get(project_id)
    mrs = project.mergerequests.list(state="opened", source_branch=branch)
    link = f"{pipeline['pages_url']}/-/jobs/{job_id}/artifacts/report.htm"
    if report == "":
        completed = ""
    else:
        completed = " with some errors"
    try:
        for mr in mrs:
            mr_note = mr.notes.create(
                {
                    "body": f"The pipeline #{pipeline['pipeline_id']} has completed{completed}.\nCheck the full report at: {link}\n{report}"
                }
            )
    except Exception as e:
        print(e, file=sys.stderr)
    return


if __name__ == "__main__":
    pipe = process_pipeline(
        os.environ["CI_SERVER_URL"],
        os.environ["CI_PROJECT_ID"],
        os.environ["CI_PIPELINE_ID"],
    )

    report_md = generate_report(pipe)
    f = open("report.md", "w")
    f.write(report_md)
    f.close()

    report_html = markdown.markdown(report_md, extensions=["toc", "fenced_code"])
    f = open("report.htm", "w")
    f.write('<link rel="stylesheet" href="vanilla.css">\n')
    f.write(report_html)
    f.close()

    report_boring = generate_boring(pipe)
    f = open("report_boring.txt", "w")
    f.write(report_boring)

    report_simple = print_broken_tests(pipe)
    f = open("report_simple.md", "w")
    f.write(report_simple)

    if "REPORTER_TOKEN" in os.environ:
        send_note(
            pipe,
            os.environ["CI_SERVER_URL"],
            os.environ["REPORTER_TOKEN"],
            os.environ["CI_PROJECT_ID"],
            os.environ["CI_JOB_ID"],
            os.environ["CI_COMMIT_REF_NAME"],
            report_simple,
        )
