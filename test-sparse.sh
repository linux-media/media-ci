#!/bin/sh
# Copyright 2024 Hans Verkuil <hverkuil@xs4all.nl>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

VALID_ARGS="--kernel_dir --kernel_config --arch --cross_compile --sparse"

# Override default
[ -z "$ARCH" ] && ARCH=x86

CI_DIR=$(dirname "$(realpath "$0")")
. "$CI_DIR"/lib/common-init.sh

if [ -r "$KERNEL_CONFIG" ]; then
  KERNEL_CONFIG=$(realpath "$KERNEL_CONFIG")
fi

cd "$KERNEL_DIR"

if [ ! -x "$SPARSE" ]; then
  echo "Unable to find the sparse binary $SPARSE"
  exit 2
fi

sh "$CI_DIR"/lib/kernel-config.sh
COMBINED_LOG="build.log" ERR_LOG="build.err" tee_log make -i -j "$(nproc)" W=1 C=1 CHECK="$SPARSE" CF="-D__CHECK_ENDIAN__ -fmemcpy-max-count=11000000" KCFLAGS="-Wmaybe-uninitialized" drivers/media/ drivers/staging/media/ drivers/input/touchscreen/ 2>&1

sh "$CI_DIR"/testdata/static/parse-sparse_log.sh build.err

. "$CI_DIR"/lib/common-end.sh
