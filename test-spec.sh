#!/bin/sh
# Copyright 2024 Hans Verkuil <hverkuil@xs4all.nl>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

VALID_ARGS="--kernel_dir --doc_target --ignore_warnings"

CI_DIR=$(dirname "$(realpath "$0")")
. "$CI_DIR"/lib/common-init.sh

cd "$KERNEL_DIR" || exit 1

sed s,userspace-api/media,media/userspace-api,g -i Documentation/Makefile
mkdir -p Documentation/media
cat <<EOF >Documentation/media/index.rst
.. SPDX-License-Identifier: GPL-2.0

.. include:: <isonum.txt>

**Copyright** |copy| 1991-: LinuxTV Developers

================================
Linux Kernel Media Documentation
================================

.. toctree::
	:maxdepth: 4

        userspace-api/index
        driver-api/index
        admin-guide/index

.. _kernel_org_trust_repository:

Lorem ipsum
------------


EOF

ln -s ../admin-guide/media Documentation/media/admin-guide
ln -s ../driver-api/media Documentation/media/driver-api
ln -s ../userspace-api/media Documentation/media/userspace-api

COMBINED_LOG="spec.log" ERR_LOG="spec.err" tee_log make SPHINXDIRS="media" "$DOC_TARGET"

if [ "$IGNORE_WARNINGS" != 0 ]; then
  . "$CI_DIR"/lib/common-end.sh
  exit 0
fi

sh "$CI_DIR"/testdata/doc/parse-spec_log.sh spec.log
. "$CI_DIR"/lib/common-end.sh
