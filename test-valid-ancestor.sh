#!/bin/sh
# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

MEDIA_REPO="git://linuxtv.org/media.git"
COMMITTERS_REPO="https://gitlab.freedesktop.org/linux-media/media-committers.git"
BRANCHES="next fixes"
VALID_ARGS="--kernel_dir"

CI_DIR=$(realpath "$(dirname "$0")")
. "$CI_DIR"/lib/common-init.sh

cd "$KERNEL_DIR"

for repo in "$MEDIA_REPO" "$COMMITTERS_REPO"; do
  for branch in $BRANCHES; do
    sha=$(git ls-remote "$repo" "$branch" | awk '{print $1;}')
    [ -z "$sha" ] && continue
    if git merge-base --is-ancestor "$sha" HEAD 2>&1; then
      echo "NOTE: Code based on $repo $branch"
      . "$CI_DIR"/lib/common-end.sh
      exit 0
    fi
  done
done

echo "ERROR: This code is not based on any of the branches: $BRANCHES" >&2
echo "       from $MEDIA_REPO or $COMMITTERS_REPO." >&2
echo "       The output of media-ci will not be reliable, and the code will not" >&2
echo "       land upstream as-is. Please rebase your code." >&2
exit 1
