#!/bin/sh
# Copyright 2024 Hans Verkuil <hverkuil@xs4all.nl>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

VALID_ARGS="--kernel_dir --kernel_config --kernel_undef --arch --cross_compile --llvm --ignore_warnings --skip_clean"

CI_DIR=$(dirname "$(realpath "$0")")
. "$CI_DIR"/lib/common-init.sh

if [ -r "$KERNEL_CONFIG" ]; then
  KERNEL_CONFIG=$(realpath "$KERNEL_CONFIG")
fi

cd "$KERNEL_DIR"

[ -z "$LLVM" ] && KCFLAGS="-Wmaybe-uninitialized"

sh "$CI_DIR"/lib/kernel-config.sh

[ -n "$KERNEL_UNDEF" ] && sh "$CI_DIR"/lib/kernel-undef.sh "$KERNEL_UNDEF"

grep ^CONFIG_CC_VERSION_TEXT= .config

COMBINED_LOG="build.log" ERR_LOG="build.err" tee_log make W=1 KCFLAGS="$KCFLAGS" -j $(( $(nproc) * 2 )) drivers/staging/media/ drivers/media/ drivers/input/touchscreen/

if [ "$IGNORE_WARNINGS" != 0 ]; then
  . "$CI_DIR"/lib/common-end.sh
  exit 0
fi

sh "$CI_DIR"/testdata/build/parse-kernel_log.sh build.err

. "$CI_DIR"/lib/common-end.sh
