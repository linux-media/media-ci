#!/bin/sh
# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

TS_DIR=$(realpath "$(dirname "$0")")

VALID_ARGS="--kernel_dir --sparse --junit_dir"
. "$TS_DIR"/lib/common-init.sh
cd "$KERNEL_DIR" || exit 1

set -v
testcase sh "$CI_DIR"/test-compile-all.sh
testcase sh "$CI_DIR"/test-smatch.sh
testcase sh "$CI_DIR"/test-sparse.sh
testcase sh "$CI_DIR"/test-coccinelle.sh
testcase sh "$CI_DIR"/test-dts-binding.sh

. "$TS_DIR"/lib/common-end.sh
