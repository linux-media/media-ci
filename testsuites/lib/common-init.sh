#!/bin/sh
# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


CI_DIR=$(realpath "${TS_DIR}/../")
. "$CI_DIR"/lib/common-init.sh

# Create default test names
TESTSUITE=$(basename "$0")
export TESTSUITE="${TESTSUITE%.*}"

[ -z "$KERNEL_DIR" ] && exit 1
cd "$KERNEL_DIR" || exit 1

# Clean old testunit
[ -z "$JUNIT_DIR" ] && exit 1
[ "$(realpath "${JUNIT_DIR}")" = / ] && exit 1
rm -rf "${JUNIT_DIR}"
mkdir -p "${JUNIT_DIR}"

get_testcase_name(){
  # Use the user provided testcase name if provided
  [ -n "$TESTCASE" ] && echo "$TESTCASE" && return 0
  # Use the first proper name and remove the extension
  # sh foo.sh -> foo
  # foo -> foo
  out="$1"
  [ "$out" = "sh" ] && out="$2"
  out=$(basename "$out")
  echo "${out%.*}"
}

testcase(){
  sh "${TS_DIR}/lib/clean.sh"
  testcase=$(get_testcase_name "$1" "$2")
  START_TIME=$(date +%s)
  COMBINED_LOG="$JUNIT_DIR/$testcase.log.txt" ERR_LOG="$JUNIT_DIR/$testcase.err.txt" python3 "$CI_DIR"/lib/tee.py "$@"
  RETCODE=$?
  RETCODE=$RETCODE JUNIT_DIR=$(realpath -m "$JUNIT_DIR" --relative-to="$PWD") TESTCASE=${testcase} START_TIME="$START_TIME" python3 "$TS_DIR"/lib/write_junit.py
  return "$RETCODE"
}
