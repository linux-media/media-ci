#!/usr/bin/python3
# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
from junitparser import JUnitXml

# Fetch the arguments via envvars
JUNIT_OUT = os.environ["JUNIT_DIR"] + "/junit.xml"

xml = JUnitXml.fromfile(JUNIT_OUT)
for testsuite in xml:
    if testsuite.errors or testsuite.failures:
        os._exit(-1)

os._exit(0)
