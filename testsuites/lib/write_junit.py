#!/usr/bin/python3
# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import datetime
from junitparser import TestCase, TestSuite, JUnitXml, Failure


def xmlscape(txt):
    # Control chars
    ctrl = list(range(32))
    ctrl.remove(10)
    ctrl.remove(13)
    txt = txt.translate(dict.fromkeys(ctrl))
    return txt


def add_file(testcase, name, path):
    if not os.path.isfile(JUNIT_ERR):
        return
    with open(path, "r", errors="replace") as file:
        txt = file.read()
    if txt == "":
        return

    # Always show full lines.
    if len(txt) > 4096:
        txt = txt[-4096:]
        pos = txt.find("\n")
        if pos >= 0:
            txt = txt[pos:]

    txt = f"[[ATTACHMENT|{path}]]\n" + xmlscape(txt)
    if name == "system_err":
        testcase.system_err = txt
    else:
        testcase.system_out = txt


# Fetch the arguments via envvars
TESTCASE = os.environ["TESTCASE"]
RETCODE = int(os.environ["RETCODE"])
TESTSUITE = os.environ["TESTSUITE"]
START_TIME = int(os.environ["START_TIME"])
JUNIT_OUT = f"{os.environ['JUNIT_DIR']}/junit.xml"
JUNIT_LOG = f"{os.environ['JUNIT_DIR']}/{TESTCASE}.log.txt"
JUNIT_ERR = f"{os.environ['JUNIT_DIR']}/{TESTCASE}.err.txt"

xml = JUnitXml()
testsuite = TestSuite(TESTSUITE)

time = datetime.datetime.today().timestamp() - START_TIME
testcase = TestCase(TESTCASE, time=time)

if RETCODE != 0:
    testcase.result = [Failure(str(RETCODE), "ERROR")]

add_file(testcase, "system_out", JUNIT_LOG)
add_file(testcase, "system_err", JUNIT_ERR)

testsuite.add_testcases([testcase])
xml.add_testsuite(testsuite)

if os.path.isfile(JUNIT_OUT):
    pre_xml = JUnitXml.fromfile(JUNIT_OUT)
    xml = pre_xml + xml
    xml.update_statistics()

xml.write(JUNIT_OUT)
