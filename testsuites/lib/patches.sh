#!/bin/sh
# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
. "$CI_DIR"/lib/patches.sh

get_fixes_gitlab(){
  patch=$1
  shas=$(get_patch_shas "$patch")
  for sha in $shas; do
    git cat-file -e "$sha" && continue
    git fetch --depth 1 "${CI_REPOSITORY_URL}" "$sha" && continue
    full_sha=$(curl -s "https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/repository/commits/${sha}" | jq -r .id)
    git fetch --depth 1 "${CI_REPOSITORY_URL}" "$full_sha"
  done
}

exit_if_merge() {
  if git rev-parse HEAD^2 >/dev/null 2>/dev/null; then
    echo "NOTE: Tree is a Merge. Skipping the test" >&2
    exit 0
  fi
}

check_patchset_size() {
  exit_if_merge

  [ -z "$GIT_ORIGIN" ] && { echo "GIT_ORIGIN not set"; exit 1; }
  [ -z "$PATCHES_MAX" ] && { echo "PATCHES_MAX not set"; exit 1; }

  echo "Using as origin $GIT_ORIGIN: $(git rev-list -n 1 "$GIT_ORIGIN")"

  if ! SKIP_CLEAN=1 TESTCASE="rebase" GIT_COMMITTER_NAME="Media CI" GIT_COMMITTER_EMAIL="mediaci@email.com" testcase git -c rebase.instructionFormat='%s%nexec GIT_COMMITTER_DATE="%cD" GIT_COMMITTER_NAME="%cn" GIT_COMMITTER_EMAIL="%ce" git commit --amend --no-edit' rebase "$GIT_ORIGIN"; then
    echo ERROR: "Failed to rebase on top of $GIT_ORIGIN." >&2
    exit 1
  fi

  patch_count="$(git rev-list --count "$GIT_ORIGIN"..HEAD)"

  if [ "$patch_count" = 0 ]; then
    echo No patches no test, exiting
    exit 0
  fi

  if [ "$patch_count" -gt "$PATCHES_MAX" ]; then
    echo "ERROR: Too many patches ($patch_count > $PATCHES_MAX) from $GIT_ORIGIN. Bailing out. ">&2
    exit 1
  fi
}

generate_patches() {
  check_patchset_size
  git format-patch "$GIT_ORIGIN" || exit 1
}
