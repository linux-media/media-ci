#!/bin/sh
# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

TS_DIR=$(realpath "$(dirname "$0")")

VALID_ARGS="--kernel_dir --git_origin --patches_max --junit_dir"
. "$TS_DIR"/lib/common-init.sh

cd "$KERNEL_DIR" || exit 1
. "$CI_DIR"/testsuites/lib/patches.sh
check_patchset_size

. "$CI_DIR"/testsuites/lib/clean.sh

set -v
init_rev=$(git rev-parse HEAD)
n_patches=$(git rev-list --count "$GIT_ORIGIN"..HEAD~1)

if [ "$n_patches" -lt 1 ]; then
  echo "Only one patch to test, exiting"
  exit 0
fi

npatch=0
for version in $(seq "$n_patches" -1 1); do
  echo "Testing $version"
  git reset --hard "$init_rev"~"$version"
  npatch=$((npatch + 1))
  prefix="$(printf "%.4d" "$npatch")-$(git rev-parse HEAD)"
  TESTCASE="$prefix bisect build" SKIP_CLEAN=1 testcase sh "$CI_DIR"/test-build.sh --ignore_warnings 1
  TESTCASE="$prefix bisect misc" SKIP_CLEAN=1 testcase sh "$CI_DIR"/test-misc.sh
done

git reset --hard "$init_rev"

. "$TS_DIR"/lib/common-end.sh
