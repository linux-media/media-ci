#!/bin/sh
# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

TS_DIR=$(realpath "$(dirname "$0")")

VALID_ARGS="--kernel_dir --junit_dir"
. "$TS_DIR"/lib/common-init.sh
cd "$KERNEL_DIR" || exit 1

set -v

TESTCASE="pahole x86" testcase sh "$CI_DIR"/test-pahole.sh --cross_compile x86_64-linux-gnu- --cross_compile32 i686-linux-gnu-
TESTCASE="pahole arm" testcase sh "$CI_DIR"/test-pahole.sh --cross_compile aarch64-linux-gnu- --cross_compile32 arm-linux-gnueabi-
TESTCASE="pahole powerpc" testcase sh "$CI_DIR"/test-pahole.sh --cross_compile powerpc64-linux-gnu- --cross_compile32 powerpc64-linux-gnu-

TESTCASE="abidump x86_64" testcase sh "$CI_DIR"/test-abi-dumper.sh --arch x86_64 --cross_compile x86_64-linux-gnu-
TESTCASE="abidump x86" testcase sh "$CI_DIR"/test-abi-dumper.sh --arch x86 --cross_compile i686-linux-gnu-
TESTCASE="abidump arm64 "testcase sh "$CI_DIR"/test-abi-dumper.sh --arch arm64 --cross_compile aarch64-linux-gnu-
TESTCASE="abidump arm" testcase sh "$CI_DIR"/test-abi-dumper.sh --arch arm --cross_compile arm-linux-gnueabi-
TESTCASE="abidump powerpc64" testcase sh "$CI_DIR"/test-abi-dumper.sh --arch powerpc --cross_compile powerpc64-linux-gnu-

. "$TS_DIR"/lib/common-end.sh
