#!/bin/sh
# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


VALID_ARGS="--kernel_dir --git_origin --patches_max --junit_dir"

TS_DIR=$(realpath "$(dirname "$0")")
. "$TS_DIR"/lib/common-init.sh

cd "$KERNEL_DIR" || exit 1

testcase sh "$CI_DIR"/test-valid-ancestor.sh

. "$CI_DIR"/testsuites/lib/patches.sh
. "$CI_DIR"/testsuites/lib/clean.sh

generate_patches
for patch in ./*.patch; do
  echo "Testing $patch"
  get_fixes_gitlab "$patch"
  SKIP_CLEAN=1 TESTCASE="$patch media style" testcase sh "$CI_DIR"/test-media-patchstyle.sh --patch "$patch"
  echo
done

. "$TS_DIR"/lib/common-end.sh
