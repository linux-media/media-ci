#!/bin/sh
# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

TS_DIR=$(realpath "$(dirname "$0")")

VALID_ARGS="--kernel_dir --junit_dir"
. "$TS_DIR"/lib/common-init.sh
cd "$KERNEL_DIR" || exit 1

for config in allmodconfig allyesconfig; do
  TESTCASE="$config x86_64" testcase sh "$CI_DIR"/test-build.sh --arch x86_64 --cross_compile x86_64-linux-gnu- --kernel_config "$config"
  [ -z "$FULL_CI" ] && break
  TESTCASE="$config x86" testcase sh "$CI_DIR"/test-build.sh --arch x86 --cross_compile i686-linux-gnu- --kernel_config "$config"
  TESTCASE="$config arm64" testcase sh "$CI_DIR"/test-build.sh --arch arm64 --cross_compile aarch64-linux-gnu- --kernel_config "$config"
  TESTCASE="$config arm" testcase sh "$CI_DIR"/test-build.sh --arch arm --cross_compile arm-linux-gnueabi- --kernel_config "$config"
  TESTCASE="$config powerpc" testcase sh "$CI_DIR"/test-build.sh --arch powerpc --cross_compile powerpc64-linux-gnu- --kernel_config "$config"
done


for config in ACPI DEBUG_FS OF PM PM_SLEEP ; do
  TESTCASE="$config x86_64" testcase sh "$CI_DIR"/test-build.sh --kernel_undef "$config"
done

. "$TS_DIR"/lib/common-end.sh
