#!/bin/sh
# Copyright 2024 Hans Verkuil <hverkuil@xs4all.nl>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

VALID_ARGS="--kernel_dir --cross_compile --virtme_script --virtme_args"

CI_DIR=$(realpath "$(dirname "$0")")
. "$CI_DIR"/lib/common-init.sh

cd "$KERNEL_DIR"

ARCH=x86_64
virtme-configkernel --defconfig --arch $ARCH 2>&1 | grep -v "reassigning to symbol"
./scripts/config -e IKCONFIG -e IKCONFIG_PROC -e DEBUG_KMEMLEAK
./scripts/config -e DEBUG_KOBJECT -e DEBUG_OBJECTS_TIMERS -e DEBUG_KOBJECT_RELEASE
./scripts/config -e MEDIA_SUPPORT -e MEDIA_TEST_SUPPORT -e V4L_TEST_DRIVERS
./scripts/config -m VIDEO_VIMC -m VIDEO_VIVID -e VIDEO_VIVID_CEC
./scripts/config -m VIDEO_VIM2M -m VIDEO_VICODEC
./scripts/config -m VIDEO_VISL -e VISL_DEBUGFS
./scripts/config -e MEDIA_DIGITAL_TV_SUPPORT -e DVB_TEST_DRIVERS
./scripts/config -e MEDIA_CONTROLLER_DVB -m DVB_VIDTV
# Hans changes
./scripts/config -d FW_CFG_SYSFS
./scripts/config -d FTRACE_SYSCALLS
./scripts/config -d DEBUG_KOBJECT
./scripts/config -d DEBUG_ENTRY
./scripts/config -d FUNCTION_TRACER
./scripts/config -d DEBUG_KMEMLEAK_AUTO_SCAN
./scripts/config -e BOOT_CONFIG
./scripts/config -e LIST_HARDENED
./scripts/config -e DEBUG_SECTION_MISMATCH
./scripts/config -e DEBUG_PAGEALLOC -e DEBUG_PAGEALLOC_ENABLE_DEFAULT
./scripts/config -e SLUB_DEBUG_ON
./scripts/config -e PAGE_TABLE_CHECK -e PAGE_TABLE_CHECK_ENFORCED
./scripts/config -e PAGE_OWNER
./scripts/config -e PAGE_POISONING
./scripts/config -e DEBUG_OBJECTS -e DEBUG_OBJECTS_FREE -e DEBUG_OBJECTS_TIMER -e DEBUG_OBJECTS_WORK
./scripts/config -e SCHED_STACK_END_CHECK
./scripts/config -e DEBUG_VM -e DEBUG_VM_IRQSOFF -e DEBUG_VM_PGFLAGS -e DEBUG_VM_PGTABLE
./scripts/config -e DEBUG_VIRTUAL
./scripts/config -e DEBUG_SHIRQ
./scripts/config -e PANIC_ON_OOPS --set-val PANIC_TIMEOUT 30
./scripts/config --set-val DEFAULT_HUNG_TASK_TIMEOUT 240
./scripts/config -e LOCKUP_DETECTOR -e SOFTLOCKUP_DETECTOR -e BOOTPARAM_SOFTLOCKUP_PANIC
./scripts/config -e HARDLOCKUP_DETECTOR -e HARDLOCKUP_DETECTOR_PREFER_BUDDY -e HARDLOCKUP_DETECTOR_COUNTS_HRTIMER -e BOOTPARAM_HARDLOCKUP_PANIC
./scripts/config -e DETECT_HUNG_TASK -e BOOTPARAM_HUNG_TASK_PANIC
./scripts/config -e WQ_WATCHDOG
./scripts/config -e SCHED_DEBUG
./scripts/config -e DEBUG_PREEMPT
./scripts/config -e DEBUG_ATOMIC_SLEEP
./scripts/config -e DEBUG_IRQFLAGS
./scripts/config -e DEBUG_KOBJECT_RELEASE
./scripts/config -e DEBUG_LIST
./scripts/config -e DEBUG_PLIST
./scripts/config -e DEBUG_SG
./scripts/config -e DEBUG_NOTIFIERS
./scripts/config -e LATENCYTOP
./scripts/config -e BOOTTIME_TRACING
./scripts/config -e FUNCTION_ERROR_INJECTION
./scripts/config -e PROVE_LOCKING
# Reduces the performance too much that breaks the tests #41
#./scripts/config -e KASAN -e KASAN_GENERIC -e KASAN_INLINE -e KASAN_STACK 

KERNEL_CONFIG=olddefconfig sh "$CI_DIR"/lib/kernel-config.sh
make all -j $(( $(nproc) * 2 ))

#TODO remove overlay-rwdir when virtme-ng upstream is fixed
virtme-run --kdir . --mods=auto --show-command --show-boot-console --verbose --cpus 2 --memory 4G --script-sh "sh $VIRTME_SCRIPT $VIRTME_ARGS" --overlay-rwdir /tmp 2>&1 | tee virtme.log

grep "^Final Summary:" virtme.log || (echo "Error running test. Final Summary not found" >&2 && exit 1)
grep "^Final Summary:" virtme.log | grep -v ", Failed: 0," >&2  && echo "Errors Found!" >&2 && exit 1
grep "^Final Summary:" virtme.log | grep -v ", Warnings: 0$" >&2 && echo "Warnings Found!" >&2 && exit 1

. "$CI_DIR"/lib/common-end.sh
