#!/bin/sh
# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

VALID_ARGS="--kernel_dir --patch"

CI_DIR=$(realpath "$(dirname "$0")")
. "$CI_DIR"/lib/common-init.sh
. "$CI_DIR"/lib/patches.sh

cd "$KERNEL_DIR"

errno=0

check_local_changelog() {
  sha=$1
  patch=$2
  msg=$(git cat-file commit "$sha")
  s=0

  if printf '%s' "$msg" | grep -q "^--- *$"; then
    echo "ERROR: $patch: Found local changelog" >&2
    s=1
  fi

  return $s
}

normalize_name() {
  name=$1
  git check-mailmap "$(echo "$name" | sed s'#\+\w*@#@#')"
}

check_signed_off_by() {
  sha=$1
  patch=$2
  msg=$(git cat-file commit "$sha")
  s=0

  author=$(echo "$msg" | grep '^author ' | head -1 | \
    cut -d ' ' -f 2- | rev | cut -d ' ' -f 3- | rev)
  if ! grep -F -q "Signed-off-by: ${author}" "$patch"; then
    echo "ERROR: $patch: Missing author ${author} Signed-off-by" >&2
    s=1
  fi

  committer=$(echo "$msg" | grep '^committer ' | head -1 | \
    cut -d ' ' -f 2- | rev | cut -d ' ' -f 3- | rev)
  if [ "$committer" = "Patchwork integration <patchwork-integration@linuxtv.org>" ]; then
    echo "INFO: Committer is Patchwork, ignoring committer test"
    return $s
  fi

  last_sob=$(grep "^Signed-off-by:" "$patch" | tail -1 | sed 's/^Signed-off-by:\s*//')
  committer=$(normalize_name "$committer")
  last_sob=$(normalize_name "$last_sob")
  if [ "${committer}" != "$last_sob" ]; then
    echo "ERROR: $patch: Committer ${committer} is not the last SoB: ${last_sob}" >&2
    s=1
  fi

  return $s
}

check_fixes() {
  sha=$1
  ret=0

  fixes_sha=$(git cat-file commit "$sha" | grep -ohE '^Fixes:\s+([0-9a-fA-F]{6,40})\b' | awk '{print $2;}')
  cc_stable=$(git cat-file commit "$sha" | grep -e '^Cc:.*stable@vger.kernel.org')
  no_stable=$(git cat-file commit "$sha" | grep -e '^Cc:.*stable+noautosel@kernel.org')
  valid_no=$(echo "$no_stable" | grep -E 'Cc: <stable\+noautosel@kernel.org> # \w+' )
  full_sha=$(git rev-parse "$sha" 2>/dev/null )

  if [ -n "$no_stable" ] && [ -z "$valid_no" ] ; then
      echo "ERROR: Commit ${full_sha} needs to provide a reason to avoid the nomination to stable" >&2
      ret=1
  fi

  if [ -n "$no_stable" ] && [ -z "$fixes_sha" ] ; then
      echo "ERROR: Commit ${full_sha} needs to provide a Fixes: tag when ccing noautosel" >&2
      ret=1
  fi

  if [ -n "$cc_stable" ] && [ -z "$fixes_sha" ] ; then
      echo "ERROR: Commit ${full_sha} needs to provide a Fixes: tag when CCing stable" >&2
      ret=1
  fi

  for fsha in $fixes_sha; do
    full_sha=$(git rev-parse "$fsha" 2>/dev/null)
    in_stable=0
    if git fetch --depth=1 git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git "$full_sha" 2>&1; then
      in_stable=1
    fi

    if [ "$in_stable" = 0 ] && [ -n "$no_stable" ]; then
      echo "ERROR: Commit ${full_sha} NOT found in the stable tree, but stable+noautosel is in Cc:" >&2
      ret=1
    fi

    if [ "$in_stable" = 1 ] && [ -z "$cc_stable" ] && [ -z "$no_stable" ] ; then
      echo "ERROR: Commit ${full_sha} found in the stable tree, but stable@vger.kernel.org not in Cc:" >&2
    fi

    if [ "$in_stable" = 0 ] && [ -n "$cc_stable" ]; then
      echo "ERROR: Commit ${full_sha} NOT found in the stable tree, but stable@vger.kernel.org is in Cc:" >&2
      ret=1
    fi
  done

  return $ret
}

check_cc() {
  sha=$1
  patch=$2
  ret=0

  for name in hverkuil mchehab linux-media; do
    in_cc=$(git cat-file commit "$sha" | grep -e "^Cc:.*$name")
    if [ -n "$in_cc" ]; then
      echo "ERROR: $patch: Don't Cc $name ($in_cc)" >&2
      ret=1
    fi
  done

  cc=$(git cat-file commit "$sha" | grep -E "^Cc: "| sed 's#^Cc:\s\+##' | sed 's# #_#g')
  for tag in Reviewed-by Acked-by Signed-off-by; do
    who=$(git cat-file commit "$sha" | grep -E "^$tag: "| sed "s%^$tag:\s\+%%" | sed 's# #_#g')
    for c in $cc; do
      for w in $who; do
        if [ "$w" = "$c" ]; then
          echo "ERROR: $patch: Don't Cc $c. They are already in $tag" >&2
          ret=1
        fi
      done
    done
  done

  if ! python3 "$CI_DIR/lib/is-dt-bindings-patch.py"; then
    return $ret
  fi

  links=$(git cat-file commit "$sha" | grep -E "^Link: "| sed "s%^Link:\s\+%%")
  for link in $links; do
    if ! b4 am --cc-trailers --single-message "$link" -n test-cc 2>&1;then
       echo "WARNING: Unable to fetch patch from $link, continuing anyway" >&2
       return $ret
    fi
    if ! grep -q -e "Cc:.*devicetree@vger.kernel.org" test-cc.mbx;then
        echo "ERROR: $patch: Please Cc: devicetree@vger.kernel.org" >&2
        ret=1
    fi
  done
  return $ret
}

check_checkpatch() {
  patch=$1

  # Commit tag warnings
  errors="BAD_FIXES_TAG"
  errors="$errors,BAD_REPORTED_BY_LINK"
  errors="$errors,BAD_SIGN_OFF"
  errors="$errors,FROM_SIGN_OFF_MISMATCH"
  errors="$errors,MISSING_FIXES_TAG"

  # License-related warnings
  errors="$errors,MODULE_LICENSE"
  errors="$errors,SPDX_LICENSE_TAG"

  # Change-Id
  errors="$errors,GERRIT_CHANGE_ID"

  # Proper Commit message
  errors="$errors,COMMIT_MESSAGE"

  # Avoid unknown shas
  errors="$errors,UNKNOWN_COMMIT_ID"

  if ! scripts/checkpatch.pl --mailback --quiet --types "$errors"  "$patch" >&2; then
    printf "\nERROR: Fails one or more mandatory checkpatch tests\n" >&2 && return 1
  fi

  return 0
}

check_subject() {
  sha=$1
  patch=$1

  subject=$(git log -n 1 --format=%s "$sha")

  if echo "$subject" | grep -E -q "^Revert"; then
    return 0
  fi

  if python3 "$CI_DIR/lib/is-misc-only-patch.py"; then
    return 0
  elif python3 "$CI_DIR/lib/is-doc-only-patch.py"; then
    if ! echo "$subject" | grep -E -q "^(doc|docs|Documentation|media):"; then
      echo "ERROR: $patch: Missing 'media:' or 'doc:' prefix in Subject" >&2 && return 1
    fi
  elif python3 "$CI_DIR/lib/is-dt-bindings-only-patch.py"; then
    if ! echo "$subject" | grep -E -q "^(dt-bindings|media):"; then
      echo "ERROR: $patch: Missing 'media:' or 'dt-bindigs:' prefix in Subject" >&2 && return 1
    fi
  elif ! echo "$subject" | grep -q "^media:"; then
    echo "ERROR: $patch: Missing 'media:' prefix in Subject" >&2 && return 1
  fi
  return 0
}

if ! python3 "$CI_DIR/lib/is-media-only-patch.py" ; then
  # shellcheck disable=SC2153
  echo "INFO: $PATCH: This is not a media patch. Ignoring" >&2
  . "$CI_DIR"/lib/common-end.sh
  exit 0
fi

sha=$(head -1 "$PATCH" | cut -d ' ' -f 2)
if ! check_local_changelog "$sha" "$PATCH"; then
  errno=1
fi

if ! check_signed_off_by "$sha" "$PATCH"; then
  errno=1
fi

if ! check_fixes "$sha"; then
  errno=1
fi

if ! check_cc "$sha" "$PATCH"; then
  errno=1
fi

if ! check_checkpatch "$PATCH"; then
  errno=1
fi

if ! check_subject "$sha" "$PATCH"; then
  errno=1
fi

[ $errno != 0 ] && exit $errno

. "$CI_DIR"/lib/common-end.sh
