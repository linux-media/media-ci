#!/bin/sh
# Copyright 2024 Hans Verkuil <hverkuil@xs4all.nl>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

VALID_ARGS="--kernel_dir"

CI_DIR=$(dirname "$(realpath "$0")")
. "$CI_DIR"/lib/common-init.sh

cd "$KERNEL_DIR"

doc_headers=include/uapi/linux/lirc.h
doc_headers="$doc_headers include/uapi/linux/videodev2.h"
doc_headers="$doc_headers include/uapi/linux/ivtv*.h"
doc_headers="$doc_headers include/uapi/linux/max2175.h"
doc_headers="$doc_headers include/uapi/linux/media*.h"
doc_headers="$doc_headers include/uapi/linux/v4l2*.h"
doc_headers="$doc_headers include/uapi/linux/uvcvideo.h"
doc_headers="$doc_headers include/uapi/linux/xilinx-v4l2-controls.h"
doc_headers="$doc_headers include/uapi/linux/ccs.h"
doc_headers="$doc_headers include/uapi/linux/smiapp.h"
doc_headers="$doc_headers include/uapi/linux/cec*.h"
doc_headers="$doc_headers "$(find include/media |grep \\\.h$)
doc_headers="$doc_headers "$(find include/uapi/linux/dvb|grep \\\.h$)
doc_headers="$doc_headers "$(find drivers/media/|grep \\\.h$)
doc_headers="$doc_headers "$(find drivers/staging/media/|grep -v atomisp|grep \\\.h$)

errno=0
for header in $doc_headers; do
  scripts/kernel-doc -Werror -none "$header" || errno=1
done

[ $errno = 0 ] || exit $errno

. "$CI_DIR"/lib/common-end.sh
