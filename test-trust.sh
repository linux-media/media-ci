#!/bin/sh
# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

VALID_ARGS="--kernel_dir --committers --core_committers --maintainers --patch"

CI_DIR=$(realpath "$(dirname "$0")")
. "$CI_DIR"/lib/common-init.sh

cd "$KERNEL_DIR"
python3 -u "$CI_DIR/lib/test-trust.py"
. "$CI_DIR"/lib/common-end.sh
