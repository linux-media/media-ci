#!/bin/sh
# Copyright 2024 Hans Verkuil <hverkuil@xs4all.nl>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

VALID_ARGS="--kernel_dir --cross_compile --cross_compile32 --cc --arch"

CI_DIR=$(dirname "$(realpath "$0")")
. "$CI_DIR"/lib/common-init.sh

cd "$KERNEL_DIR" || exit 1

headers_dir=out/usr
make -j INSTALL_HDR_PATH=$headers_dir headers_install >/dev/null

"$CROSS_COMPILE$CC" "$CI_DIR"/testdata/pahole/pahole.c -c -g -o pahole64.o -I $headers_dir/include
"$CROSS_COMPILE32$CC" "$CI_DIR"/testdata/pahole/pahole.c -c -g -o pahole32.o -I $headers_dir/include

pahole pahole64.o > pahole64.pa
pahole pahole32.o > pahole32.pa

if ! cmp pahole64.pa pahole32.pa ; then
  echo 32 vs 64 bit differences >&2
  exit 1
fi

for pafile in pahole32.pa pahole64.pa; do
  if grep -E 'hole|padding:' "$pafile"; then
    echo holes or padding in "$pafile" >&2
    exit 1
  fi
done

. "$CI_DIR"/lib/common-end.sh
