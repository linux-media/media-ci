#!/bin/sh
# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

docker_tag_push()
{
  image=$1
  tag=$2
  docker tag "$image" "$image":"$tag"
  if [ "$do_skip_push" != 1 ]; then
    docker push "$image":"$tag"
  fi
}

usage()
{
  echo "Usage build.sh --release --skip_push --no_cache"
}

CI_REGISTRY_IMAGE="${CI_REGISTRY_IMAGE:=registry.freedesktop.org/linux-media/media-ci}"
CI_COMMIT_SHA="${CI_COMMIT_SHA:=$(git rev-parse HEAD)}"

while [ $# != 0 ]; do
  case $1 in
    --release) do_release=1;;
    --skip_push) do_skip_push=1;;
    --no_cache) do_no_cache=1;;
    --help) usage && exit 1;;
  esac
  shift;
done

top=$(dirname "$(realpath "$0")")
for dockerfile in "${top}"/*/Dockerfile; do
  name=$(basename "$(dirname "$dockerfile")")
  image="$CI_REGISTRY_IMAGE/$name"
  echo "Building $dockerfile as $image"
  if [ "$do_release" = 1 ]; then
    docker buildx build --pull --cache-to type=inline -t "$image" -f "$dockerfile" .
  elif [ "$do_no_cache" = 1 ]; then
    docker build --pull -t "$image" -f "$dockerfile" .
  else
    docker buildx build --pull --cache-from type=registry,ref="$image:latest" --cache-to type=inline -t "$image" -f "$dockerfile" .
  fi
  docker_tag_push "$image" "$CI_COMMIT_SHA"
  if [ "$do_release" = 1 ]; then
    docker_tag_push "$image" latest
    docker_tag_push "$image" main
    docker_tag_push "$image" "$(date -I)"
  fi
done
