#!/bin/sh
# Copyright 2024 Hans Verkuil <hverkuil@xs4all.nl>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

VALID_ARGS="--kernel_dir --cross_compile --cc --arch --abi_reference"

CI_DIR=$(dirname "$(realpath "$0")")
. "$CI_DIR"/lib/common-init.sh

ABI_REFERENCE=$(realpath "$ABI_REFERENCE")
cd "$KERNEL_DIR" || exit 1

cat >abi-test.c <<'EOF'
#include <linux/time.h>
#include <linux/string.h>
typedef unsigned long           uintptr_t;
#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include <linux/lirc.h>
#include <linux/media.h>
#include <linux/cec.h>
#include <linux/videodev2.h>
#include <linux/v4l2-subdev.h>
#include <linux/dvb/audio.h>
#include <linux/dvb/ca.h>
#include <linux/dvb/dmx.h>
#include <linux/dvb/frontend.h>
#include <linux/dvb/net.h>
#include <linux/dvb/osd.h>
#include <linux/dvb/video.h>

union {
EOF

cat include/uapi/linux/videodev2.h include/uapi/linux/v4l2-subdev.h include/uapi/linux/media.h include/uapi/linux/cec.h include/uapi/linux/lirc.h include/uapi/linux/dvb/*.h | perl -ne 'if (m/\#define\s+([A-Z][A-Z0-9_]+)\s+\_IO.+\(.*\,\s*([^\)]+)\)/) {print "$2 t_$2;\n"}' | grep -E -v '^(int |unsigned |__)' | sed 's/t_struct /t_/' | LC_COLLATE=C sort | uniq >>abi-test.c
echo '} x;' >>abi-test.c

ln -s asm-generic include/uapi/asm
ln -s asm-generic include/asm
"$CROSS_COMPILE$CC" -g -Og -c abi-test.c -D__KERNEL__ -D__DVB_CORE__ -I include/uapi -I include

abi-dumper -quiet abi-test.o
abi-compliance-checker -l abi-test.o -old "$ABI_REFERENCE" -new ABI.dump

. "$CI_DIR"/lib/common-end.sh
