#!/bin/sh
# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

top=$(dirname "$(realpath "$0")")
cd "$top"

V4L_UTILS_REPO="${V4L_UTILS_REPO:=git://linuxtv.org/v4l-utils.git@master}"

REPO=$(echo "$V4L_UTILS_REPO" | sed -E 's#(.*)@([^@]*)#\1#')
REF=$(echo "$V4L_UTILS_REPO" | sed -E 's#(.*)@([^@]*)#\2#')

echo "Cloning $V4L_UTILS_REPO into $top"

rm -rf v4l-utils
git clone "$REPO" v4l-utils
cd v4l-utils
git checkout "$REF"
meson setup -Dv4l2-compliance-32=true -Dv4l2-ctl-32=true -Dprefix="$top/v4l-utils/build/usr" -Dudevdir="$top/v4l-utils/build/usr/lib/udev" -Dsystemdsystemunitdir="$top/v4l-utils/build/usr/lib/systemd" build/
ninja -C build/
ninja -C build install
