#!/bin/bash
# Copyright 2024 Hans Verkuil <hverkuil@xs4all.nl>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

CROSS=$1
CPUS=$(nproc)
archs="x86_64-linux-gnu i686-linux-gnu aarch64-linux-gnu arm-linux-gnueabi powerpc64-linux-gnu"
gcc_vers=14.1.0
binutils_vers=2.42

top=$(realpath "$PWD")

if [ -z "$1" ]; then
	echo "Usage: cross.sh arch|all|build-clean|clean|setup"
	echo
	echo "clean: delete the build files in the cross directory"
	echo "build-clean: delete the temporary build directories in the cross directory"
	echo "setup: download gcc/binutils and set up the build environment"
	echo
	echo "arch|all: build a cross-compilation environment for the specified"
	echo "       architecture or all of them."
	echo
	echo "available architectures: $archs"
	exit 0
fi

if [ "$1" = "clean" ]; then
	rm -rf cross/gcc* cross/binutils* cross/packages cross/bin
	exit 0
fi

if [ "$1" = "build-clean" ]; then
	rm -rf cross/gcc* cross/binutils* cross/packages
	exit 0
fi

if [ "$1" = "setup" ]; then
	mkdir -p cross/bin
	mkdir -p cross/packages
	cd cross/packages
	wget -c ftp://ftp.gnu.org/gnu/binutils/binutils-$binutils_vers.tar.xz
	wget -c ftp://ftp.gnu.org/gnu/gcc/gcc-$gcc_vers/gcc-$gcc_vers.tar.xz
	exit 0
fi

if [ "$1" = "all" ]; then
	for i in $archs
	do
		sh "$0" "$i"
	done
	exit 0
fi

cd cross

if [ ! -d binutils-$binutils_vers ]; then
	tar xaf packages/binutils-$binutils_vers.tar.xz
fi
if [ ! -d gcc-$gcc_vers ]; then
	tar xaf packages/gcc-$gcc_vers.tar.xz
fi

echo "Cross compile for: $CROSS"

rm -rf gcc-build
mkdir gcc-build
cd gcc-build
../gcc-"$gcc_vers"/configure --target="$CROSS" --prefix="$top"/cross/bin/"$CROSS" --disable-shared --disable-threads --enable-languages=c --with-newlib
make all-gcc -j"$CPUS"
make install-gcc
cp ../gcc-"$gcc_vers"/gcc/cp/cp-trait.def "$top"/cross/bin/"$CROSS"/lib/gcc/"$CROSS"/"$gcc_vers"/plugin/include/cp/cp-trait.def
cd ..
rm -rf binutils-build
mkdir binutils-build
cd binutils-build
../binutils-"$binutils_vers"/configure --target="$CROSS" --prefix="$top"/cross/bin/"$CROSS" --disable-werror
make -j"$CPUS"
make install
