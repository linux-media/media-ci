#!/bin/sh
# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

top=$(dirname "$(realpath "$0")")
cd "$top"

COMMITTERS_REPO="${COMMITTERS_REPO:=git://git.linuxtv.org/committers.git@master}"

REPO=$(echo "$COMMITTERS_REPO" | sed -E 's#(.*)@([^@]*)#\1#')
REF=$(echo "$COMMITTERS_REPO" | sed -E 's#(.*)@([^@]*)#\2#')

echo "Cloning $COMMITTERS_REPO into $top"

rm -rf committers
git clone -b "$REF" "$REPO" committers
