#!/usr/bin/python3
# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import datetime
import dateutil
import gitlab
import sys


def main(server, project_id, weeks_ago):
    created_after_txt = (
        (datetime.datetime.now() - datetime.timedelta(weeks=weeks_ago + 10))
        .astimezone()
        .isoformat()
    )
    mr_after = (
        datetime.datetime.now() - datetime.timedelta(weeks=weeks_ago)
    ).astimezone()

    gl = gitlab.Gitlab(server)
    project = gl.projects.get(project_id)
    mrs = project.mergerequests.list(
        state="merged",
        order_by="updated_at",
        created_after=created_after_txt,
        get_all=True,
        target_branch="next",
    )
    for mr in reversed(mrs):
        mr_date = dateutil.parser.parse(mr.merged_at)
        if mr_date < mr_after:
            continue
        print(f"{mr.id}: {mr.title} {mr.merged_at}\n\n {mr.description}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="mr_report", description="Produces a report for merge requests"
    )
    parser.add_argument("--server", default="https://gitlab.freedesktop.org/")
    parser.add_argument("--project_id", type=int, default=22111)
    parser.add_argument("--weeks_ago", type=int, default=10)
    args = parser.parse_args()

    ret = main(args.server, args.project_id, args.weeks_ago)
    sys.exit(ret)
