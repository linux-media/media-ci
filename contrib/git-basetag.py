#!/usr/bin/env python3
# pylint: disable=R0912,R0914,R1702,C0103
# Copyright(c) 2024: Mauro Carvalho Chehab <mchehab@kernel.org>.
# SPDX-License-Identifier: GPL-2.0 OR Apache-2.0

"""
Return the tag from where a git log was produced.

Git currently doesn't provide an internal mechanism to identify
what baseline should be used when a pull request is received.

This causes patchwork integration to fail, as they may be against
different branches/tags than the latest next or fixes branch.

The gitBaseTag class allow getting it quickly using standard
git commands. It can be used inside media-ci with:

	from git-basetag import gitBaseTag

	git_base = gitBaseTag(verbose=0)
	tags = git_base.get_all_tags(since=30)
	tag = git_base.base_tag(tags, base_commit, final_commit)

The above works with a committed branch. Alternatively, a string with
the entire commit series can be used instead with:

	tag = base_tag_from_output(tags, pr_string)

Where `verbose`, `since` and `final_commit` being optional arguments.

`since` can be a number of days ago or a string in iso format,
like `2024-08-01`.

To make easier to use, the script also contains a main() method,
allowing it to be used either standalone or included as a library.

When used standalone, it will give the tag from where the code
was applied:

	$ git-basetag.py HEAD~3
	Base tag: refs/tags/v6.10

	$ git-basetag.py maintainers-doc~3 maintainers-doc
	Base tag: refs/tags/media/v6.13-1

	$ git-basetag.py HEAD~ --since 2024-08-01
	Base tag: refs/remotes/origin/mr_report

	$ git-basetag.py HEAD~ --since 3
	Base tag: refs/remotes/origin/mr_report

	$ git-basetag.py HEAD~ --since 1
	Can't find a tag that contains all indexes

	$ git show --pretty=email | contrib/git-basetag.py --input -
        Base tag: refs/remotes/mchehab/main

Please notice that this only works if the files inside the
tested branch weren't modified since the last commit at the
tag.
"""

import argparse
import re
import subprocess
import sys

from datetime import date, datetime, timedelta


class gitBaseTag:
    """
    Identifies the more recent tag from a git branch.
    """

    def __init__(self, verbose=0):
        self.verbose = verbose

    def _run_cmd(self, cmd):
        """
        Execute an arbitrary command, handling errors.
        """

        if isinstance(cmd, list):
            cmd_list = cmd
            cmd = " ".join(cmd_list)
        else:
            cmd_list = cmd.split(" ")

        if self.verbose > 1:
            print(f"$ {cmd}")

        try:
            rc = subprocess.run(cmd_list, check=True, text=True, capture_output=True)

            if self.verbose > 2:
                print("  " + re.sub("\n", "\n  ", rc.stdout))

        except subprocess.SubprocessError:
            return ""

        return rc.stdout

    def get_all_tags(self, since=None):
        """
        Get a list of all tags
        """

        tags = []
        re_ref = re.compile(r"(\S+)\s(\S+)")

        cmd = [
            "git",
            "for-each-ref",
            "--sort=creatordate",
            "--format",
            "%(refname) %(committerdate:format:%Y-%m-%d)",
            "refs/",
        ]
        out = self._run_cmd(cmd)

        if since:
            try:
                d1 = date.fromisoformat(since)
            except ValueError:
                if isinstance(since, str):
                    try:
                        days_ago = int(since)
                    except ValueError:
                        sys.exit(
                            "since needs to be either a date or a number of days ago"
                        )
                elif isinstance(since, int):
                    days_ago = since
                else:
                    sys.exit("Unsupported format for since")

                today = datetime.date(datetime.now())
                d1 = today - timedelta(days=days_ago)

            if self.verbose:
                print(f"Since: {d1}")

        for ln in out.split("\n"):
            if match := re_ref.match(ln):
                tag = match.group(1)

                if not since:
                    tags.append(tag)
                else:
                    d2 = date.fromisoformat(match.group(2))
                    if d2 >= d1:
                        tags.append(tag)

        tags.reverse()

        if self.verbose:
            print(f"number of tags to search: {len(tags)}")

        return tags

    def base_tag_from_output(self, tags, pr_string):
        """Get a tag from an pr_string string with the entire patch series"""

        re_index = re.compile(r"index\s+([a-f\d]+)\.\.")
        re_diff = re.compile(r"diff\s+(\-\-git)? a/(\S+)")
        re_blob = re.compile(r"\d+\s+blob\s+([a-f\d]+)")

        indexes = {}

        fname = None
        for ln in pr_string.split("\n"):
            if match := re_diff.match(ln):
                fname = match.group(2)

            if match := re_index.match(ln):
                indexes[fname] = match.group(1)
                fname = None

        for tag in tags:
            return_tag = True
            for fname, index in indexes.items():
                if self.verbose > 1:
                    print(f"File {fname} index: {index}")

                cmd = f"git ls-tree {tag} {fname}"
                pr_string = self._run_cmd(cmd)
                for ln in pr_string.split("\n"):
                    if match := re_blob.match(ln):
                        tag_idx = match.group(1)
                        if not tag_idx.startswith(index):
                            return_tag = False
                            break

            if return_tag:
                return tag

        return None

    def base_tag(self, tags, base_commit, final_commit=""):
        """Get a tag that contains all indexes from a committed branch"""

        cmd = f"git log -p {base_commit}..{final_commit}"
        out = self._run_cmd(cmd)

        return self.base_tag_from_output(tags, out)


def main():
    """Main routine"""

    # On a git post-receive hook, there are three arguments:
    # oldrev, newrev, refname.
    # It probably makes sense to use, instead, an update hook, though

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-v", "--verbose", action="count", default=0, help="verbosity level"
    )
    parser.add_argument(
        "base_commit", type=str, nargs="?", help="Initial commit of the pull"
    )
    parser.add_argument(
        "final_commit", type=str, nargs="?", help="Initial commit of the pull"
    )
    parser.add_argument("--tags", type=str, nargs="+", help="Tags to be tested")
    parser.add_argument(
        "--since", type=str, help="Since a date or since a number of days ago"
    )
    parser.add_argument(
        "--input", type=str, help='Patch series input file. Can be "-" for stdin'
    )

    args = parser.parse_args()

    git_base = gitBaseTag(args.verbose)

    if not args.tags:
        args.tags = git_base.get_all_tags(since=args.since)

    if not args.final_commit:
        args.final_commit = ""

    if args.input:
        data = ""
        if args.input == "-":
            for ln in sys.stdin:
                data += ln
        else:
            with open(args.input, "r", encoding="utf-8") as fp:
                for ln in fp:
                    data += ln

        tag = git_base.base_tag_from_output(args.tags, data)
    else:
        if not args.base_commit:
            sys.exit("Script needs at least a base_commit")

        tag = git_base.base_tag(
            args.tags, args.base_commit, final_commit=args.final_commit
        )
    if not tag:
        sys.exit("Can't find a tag that contains all indexes")

    print(f"Base tag: {tag}")


if __name__ == "__main__":
    main()
