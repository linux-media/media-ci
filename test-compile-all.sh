#!/bin/sh
# Copyright 2024 Hans Verkuil <hverkuil@xs4all.nl>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

VALID_ARGS="--kernel_dir"

CI_DIR=$(dirname "$(realpath "$0")")
. "$CI_DIR"/lib/common-init.sh

cd "$KERNEL_DIR"

make ARCH=x86_64 allyesconfig
# shellcheck disable=SC2046
configs=$(grep -E "(^config |^menuconfig )" $(find drivers/staging/media/ -name Kconfig) $(find drivers/media/ -name Kconfig)  | cut -d' ' -f 2)

errno=0
for config in $configs; do
  case $config in
    MEDIA_HIDE_ANCILLARY_SUBDRV) continue;;
    RADIO_WL128X) continue;;
    VIDEO_TEGRA_TPG) continue;;
  esac
  state=$(./scripts/config -k -s "$config")
  if [ "$state" = n ] || [ "$state" = undef ]; then
    echo Unable to build "$config" >&2
    errno=1
  fi
done
[ $errno = 0 ] || exit $errno

. "$CI_DIR"/lib/common-end.sh
