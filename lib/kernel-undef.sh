#!/bin/sh
# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

[ -z "$1" ] && exit 1

CONFIG="$1"

scripts/config -d "$CONFIG"

case "$CONFIG" in
  DEBUG_FS)
    scripts/config -d KUNIT_ALL_TESTS
    scripts/config -d ZSMALLOC_STAT
    scripts/config -d VCAP_KUNIT_TEST
    scripts/config -d EDAC_DEBUG
    scripts/config -d XFS_ONLINE_SCRUB_STATS
    scripts/config -d SUNRPC_DEBUG
    scripts/config -d DEBUG_CLOSURES
    scripts/config -d DEBUG_KMEMLEAK
    scripts/config -d PUNIT_ATOM_DEBUG
    scripts/config -d NOTIFIER_ERROR_INJECTION
    scripts/config -d FAULT_INJECTION
    scripts/config -d FAIL_FUTEX
    scripts/config -d BLK_DEV_IO_TRACE
    scripts/config -d PAGE_OWNER
    scripts/config -d GPIO_VIRTUSER
    scripts/config -d KCOV
    ;;
  PM|PM_SLEEP)
    scripts/config -d SUSPEND
    scripts/config -d XEN
    scripts/config -d HIBERNATION
    ;;
  MITIGATION_RETPOLINE)
    scripts/config -d GCC_PLUGINS # Required by ancient gcc
    ;;
esac

make olddefconfig

case $(scripts/config -s "$CONFIG") in
  y|m)
  echo "Unable to undef $CONFIG" >&2
  exit 1
  ;;
esac

exit 0
