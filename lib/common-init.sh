#!/bin/sh
# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


indirect() {
  eval "echo -n \"\$$1\""
}

get_param() {
  param="$1"
  query="$2"

  case "$param" in
    --abi_reference)
      helpmsg="Abi-dump reference file"
      [ -z "$ARCH" ] && ARCH=x86_64
      defval="$CI_DIR/testdata/abi/abi.$ARCH.dump"
      envvar=ABI_REFERENCE
      ;;
    --arch)
      helpmsg="Kernel architecture"
      defval=x86_64
      envvar=ARCH
      ;;
    --cc)
      helpmsg="Compiler"
      defval=gcc
      envvar=CC
      ;;
    --committers)
      helpmsg="File containing committers"
      defval="$CI_DIR/third_party/committers/committers.txt"
      envvar=COMMITTERS
      ;;
    --core_committers)
      helpmsg="File containing core committers"
      defval="$CI_DIR/third_party/committers/core_committers.txt"
      envvar=CORE_COMMITTERS
      ;;
    --cross_compile)
      helpmsg="Cross-compile prefix"
      defval=x86_64-linux-gnu-
      envvar=CROSS_COMPILE
      ;;
    --cross_compile32)
      helpmsg="Cross-compile prefix for 32-bit ABI-checks comparison"
      defval=i686-linux-gnu-
      envvar=CROSS_COMPILE32
      ;;
    --doc_target)
      helpmsg="Target when building documentation"
      defval=htmldocs
      envvar=DOC_TARGET
      ;;
    --do_setup)
      helpmsg="Setup the required dependencies"
      defval=0
      envvar=DO_SETUP
      ;;
    --git_origin)
      helpmsg="GIT ref used for reference"
      defval=origin/master
      envvar=GIT_ORIGIN
      ;;
    --ignore_warnings)
      helpmsg="Ignore warning during builds"
      defval=0
      envvar=IGNORE_WARNINGS
      ;;
    --junit_dir)
      helpmsg="Output directory for junit logs and reports"
      defval="$PWD/junit"
      envvar=JUNIT_DIR
      ;;
    --kernel_dir)
      helpmsg="Path of the kernel directory"
      defval="$PWD"
      envvar=KERNEL_DIR
      ;;
    --kernel_config)
      helpmsg="Kernel configuration or config file"
      defval=allyesconfig
      envvar=KERNEL_CONFIG
      ;;
    --kernel_undef)
      helpmsg="Kernel option to be removed from config"
      defval=""
      envvar=KERNEL_UNDEF
      ;;
    --llvm)
      helpmsg="Enable llvm for kernel builds"
      defval=
      envvar=LLVM
      ;;
    --maintainers)
      helpmsg="File containing subsystem maintainers"
      defval="$CI_DIR/third_party/committers/maintainers.txt"
      envvar=MAINTAINERS
      ;;
    --patch)
      helpmsg="Patch file to test"
      defval="test.patch"
      envvar=PATCH
      ;;
    --patches_max)
      helpmsg="Max number of patches to be analyzed"
      defval=100
      envvar=PATCHES_MAX
      ;;
    --skip_clean)
      helpmsg="Skip clean after successful test"
      defval=0
      envvar=SKIP_CLEAN
      ;;
    --smatch)
      helpmsg="Smatch analyzer"
      defval="$CI_DIR/third_party/smatch/smatch"
      envvar=SMATCH
      ;;
    --sparse)
      helpmsg="Sparse analyzer"
      defval="$CI_DIR/third_party/smatch/sparse"
      envvar=SPARSE
      ;;
    --virtme_args)
      helpmsg="Arguments for the virme script"
      defval="$CI_DIR/third_party/v4l-utils \"mc -kmemleak\""
      envvar=VIRTME_ARGS
      ;;
    --virtme_script)
      helpmsg="Script file executed by virtme"
      defval="$CI_DIR/testdata/virtme/test.sh"
      envvar=VIRTME_SCRIPT
      ;;
    *)
      echo "Unknown parameter $param"
      exit 1
      ;;
  esac
  case "$query" in
    helpmsg|defval|envvar) indirect "$query";;
    *) echo "Unknown query $query" && exit 1;;
  esac
}

usage() {
  printf "Usage: %s [--help]" "$ci_script"
  for arg in $VALID_ARGS; do
    printf " [%s value]" "$arg"
  done
  echo; echo

  echo "Arguments:"
  printf " --help: show this help\n"
  for arg in $VALID_ARGS; do
    helpmsg=$(get_param "$arg" helpmsg)
    defval=$(get_param "$arg" defval)
    printf " %s %s: %s\n" "$arg" "$defval" "$helpmsg"
  done

  echo
  echo "Environment Variables:"
  for arg in $VALID_ARGS; do
    envvar=$(get_param "$arg" envvar)
    printf " %s: %s\n" "$arg" "$envvar"
  done
}

tee_log() {
  python3 "$CI_DIR"/lib/tee.py "$@"
}

if [ -z "$VALID_ARGS" ] || [ -z "$CI_DIR" ]; then
  echo "VALID_ARGS and CI_DIR are not set. Exiting!"
  exit 2
fi

ci_script=$(basename "$0")
CI_COMMAND="$0 $*"
export CI_COMMAND

while [ $# != 0 ]; do
  valid=0

  for arg in $VALID_ARGS "-h" "--help"; do
    if [ "$1" = "$arg" ]; then
      valid=1
      break
    fi
  done

  if [ $valid = 0 ]; then
    echo "Invalid argument $1" && exit 1
  fi

  case $1 in
    --help|-h) usage && exit 0;;
  esac

  envvar=$(get_param "$arg" envvar) || exit 1
  export "$envvar"="$2"
  shift; shift
done

for arg in $VALID_ARGS; do
  envvar=$(get_param "$arg" envvar)
  [ -n "$(indirect "$envvar")" ] && continue
  defval=$(get_param "$arg" defval)
  export "$envvar"="$defval"
done
