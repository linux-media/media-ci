#!/usr/bin/python3
# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import subprocess, sys, os, time

COMBINED_LOG = os.environ["COMBINED_LOG"]
ERR_LOG = os.environ["ERR_LOG"]


def print_log(pipe_in, files, tty):
    while True:
        buf = pipe_in.read1()
        if not buf:
            return
        for file in files:
            file.write(buf)
        print(buf.decode("utf-8", "replace"), end="", file=tty)


def run_tee(command_args, combined, stderr):
    proc = subprocess.Popen(
        command_args,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        stdin=subprocess.DEVNULL,
        close_fds=True,
    )

    os.set_blocking(proc.stdout.fileno(), False)
    os.set_blocking(proc.stderr.fileno(), False)
    while True:
        print_log(proc.stdout, [combined], sys.stdout)
        print_log(proc.stderr, [combined, stderr], sys.stderr)
        if proc.poll() is not None:
            break
        time.sleep(0.1)
    return proc.returncode


combined = open(COMBINED_LOG, "wb")
stderr = open(ERR_LOG, "wb")

ret = run_tee(sys.argv[1:], combined, stderr)
sys.exit(ret)
