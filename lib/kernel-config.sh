#!/bin/sh
# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

set -e

[ -z "$KERNEL_CONFIG" ] && exit 1

# File kernel config.
if [ -r "$KERNEL_CONFIG" ]; then
  cp "$KERNEL_CONFIG" .config
  make olddefconfig
  exit 0
fi

make "$KERNEL_CONFIG"

case "$KERNEL_CONFIG" in
  allyesconfig)
  config_opt=-e
  ;;
  allmodconfig)
  config_opt=-m
  ;;
  *)
  exit 0
  ;;
esac

if [ "$ARCH" = x86 ];then
  scripts/config -d 64BIT
fi

scripts/config -d STAGING_EXCLUDE_BUILD -d KCOV -d WERROR

# Disable all touchscreen drivers, except the ones using v4l2.
perl -p -i -e 's/CONFIG_TOUCHSCREEN_(.*)=.*/CONFIG_TOUCHSCREEN_\1=n/' .config
scripts/config "$config_opt" TOUCHSCREEN_SUR40
scripts/config "$config_opt" TOUCHSCREEN_ATMEL_MXT
scripts/config -e TOUCHSCREEN_ATMEL_MXT_T37

make olddefconfig
