#!/usr/bin/python3
# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import subprocess, os, re, sys, fnmatch
from media_paths import (
    get_modified_files,
    MEDIA_PATHS,
    MEDIA_CORE_PATHS,
    DT_PATHS,
    MISC_PATHS,
)

MIN_COMMITTERS = 1
MIN_COMMITTERS_CORE = 2


def name_replaces(user):
    user = user.replace("<k.kozlowski.k@gmail.com>", "<krzk@kernel.org>")
    user = user.replace("<k.kozlowski@samsung.com>", "<krzk@kernel.org>")
    user = user.replace("<krzysztof.kozlowski@canonical.com>", "<krzk@kernel.org>")
    user = user.replace("<krzysztof.kozlowski@linaro.org>", "<krzk@kernel.org>")
    user = user.replace("Rob Herring (Arm)", "Rob Herring")
    user = user.replace("<robherring2@gmail.com>", "<robh@kernel.org>")
    user = user.replace("<rob.herring@calxeda.com>", "<robh@kernel.org>")
    user = user.replace("<conor@kernel.org>", "<conor@kernel.org>")
    user = user.replace("<conor.dooley@microchip.com>", "<conor@kernel.org>")
    user = user.replace("<mail@conchuod.ie>", "<conor@kernel.org>")
    return user


def get_canonical_user(user):
    user = re.sub(r"\+\w+@", "@", user)
    user = re.sub(r">.*", ">", user)
    user = name_replaces(user)
    sp = subprocess.run(["git", "check-mailmap", user], capture_output=True)
    if sp.returncode != 0:
        return user
    return sp.stdout.decode("utf-8").rstrip()


def get_userlist(file):
    return {get_canonical_user(line.rstrip()) for line in open(file).readlines()}


def get_userlist_kernel(path):
    sp = subprocess.run(
        [
            "./scripts/get_maintainer.pl",
            path,
            "--no-git-blame",
            "--no-git-fallback",
            "--no-l",
            "--no-rolestats",
        ],
        capture_output=True,
    )
    if sp.returncode != 0:
        return {}
    return {
        get_canonical_user(line.rstrip())
        for line in sp.stdout.decode("utf-8").splitlines()
    }


def get_users_with_tag(commit_txt, tag):
    out = []
    for line in commit_txt.splitlines():
        if not line.startswith(tag):
            continue
        _, user = line.rstrip().split(": ", 1)
        user = get_canonical_user(user)
        out.append(user)
    return out


def get_patch_reviewers(patch):
    commit_txt, _ = patch.rsplit("\n---", 1)
    patch_reviewers = set()
    for tag in "Signed-off-by", "Reviewed-by", "Acked-by":
        patch_reviewers |= set(get_users_with_tag(commit_txt, tag))

    return patch_reviewers


def get_patch_author(patch):
    commit_txt, _ = patch.rsplit("\n---", 1)
    return get_users_with_tag(commit_txt, "From:")[0]


def check_patch_trust(patch_reviewers, reviewers, min_reviewers, verbose=True):
    valid_patch_reviewers = patch_reviewers & reviewers
    valid = len(valid_patch_reviewers) >= min_reviewers

    if not verbose:
        return valid

    print(f"Patch reviewed by: {patch_reviewers}")
    print(f"Valid reviewers in the patch: {valid_patch_reviewers}")
    if valid:
        print(f"Trust check PASS")
        return valid
    else:
        print(
            f"ERROR: We need at least {min_reviewers} reviewers, but we only got {len(valid_patch_reviewers)}",
            file=sys.stderr,
        )
    return valid


def all_files_in_list(files, match_list):
    ret = True
    for file in files:
        for match in match_list:
            if fnmatch.fnmatch(file, match):
                break
        else:
            print(f"ERROR: File {file} is Unknown", file=sys.stderr)
            ret = False
    return True


def any_file_in_list(files, match_list, verbose=True):
    for file in files:
        for match in match_list:
            if fnmatch.fnmatch(file, match):
                return True

    return False


patch = open(os.environ["PATCH"]).read()
committers = get_userlist(os.environ["COMMITTERS"])
core_committers = get_userlist(os.environ["CORE_COMMITTERS"])
maintainers = get_userlist(os.environ["MAINTAINERS"])
dt_maintainers = get_userlist_kernel("Documentation/devicetree/bindings/")


modified_files = get_modified_files(patch)
patch_reviewers = get_patch_reviewers(patch)
patch_author = get_patch_author(patch)

ret = 0
if not all_files_in_list(modified_files, MEDIA_PATHS + DT_PATHS + MISC_PATHS):
    ret += 1

# For every patch we only need one committer different than the author
if not check_patch_trust(
    patch_reviewers - set((patch_author,)),
    committers | core_committers | maintainers,
    MIN_COMMITTERS,
):
    ret += 2

# For core patches drivers we need two core committers
if any_file_in_list(modified_files, MEDIA_CORE_PATHS):
    print("Checking Media-core rules:")
    if not check_patch_trust(
        patch_reviewers,
        core_committers | maintainers,
        MIN_COMMITTERS_CORE,
    ):
        ret += 4

# For DT patches we need one dt maintainer
if any_file_in_list(modified_files, DT_PATHS):
    print("Checking DT rules:")
    if not check_patch_trust(patch_reviewers, dt_maintainers, MIN_COMMITTERS):
        ret += 8

os._exit(ret)
