#!/usr/bin/python3
# Copyright 2024 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

MEDIA_PATHS = (
    "Documentation/admin-guide/media/*",
    "Documentation/driver-api/media/*",
    "Documentation/userspace-api/*",
    "drivers/media/*",
    "drivers/staging/media/*",
    "include/linux/platform_data/media/*",
    "include/linux/usb/uvc.h",
    "include/media/*",
    "include/uapi/linux/media*",
    "include/uapi/linux/usb/video.h",
    "include/uapi/linux/uvcvideo.h",
    "include/uapi/linux/v4l2-*",
    "include/uapi/linux/videodev2.h",
    "samples/v4l/v4l2-pci-skeleton.c",
)

MEDIA_CORE_PATHS = (
    "drivers/media/common/videobuf2/*",
    "drivers/media/dvb-core/*",
    "drivers/media/cec/core/*",
    "drivers/media/mc/*",
    "drivers/media/v4l2-core/*",
    "drivers/media/rc/lirc_dev.c",
    "drivers/media/rc/rc-core-priv.h",
    "drivers/media/rc/rc-main.c",
    "include/media/cec*",
    "include/media/media-*",
    "include/media/v4l2-*",
    "include/media/videobuf2-*",
    "include/uapi/linux/cec*",
    "include/uapi/linux/media.h",
    "include/uapi/linux/media-bus-format.h",
    "include/uapi/linux/usb/video.h",
    "include/uapi/linux/uvcvideo.h",
    "include/uapi/linux/videodev2.h",
    "include/uapi/linux/v4l2-*",
)

DT_PATHS = ("Documentation/devicetree/bindings/*",)

MISC_PATHS = ("MAINTAINERS", ".mailmap")

DOC_PATHS = (
    "Documentation/admin-guide/media/*",
    "Documentation/driver-api/media/*",
    "Documentation/userspace-api/*",
)

import fnmatch


def is_media_file(file, paths=MEDIA_PATHS):
    for path in paths:
        if fnmatch.fnmatch(file, path):
            return True
    return False


def get_modified_files(patch):
    if patch.startswith("diff --git"):
        diff = patch
    else:
        _, diff = patch.rsplit("\n---\n", 1)
    out = set()
    prefix = "diff --git a/"
    for line in diff.splitlines():
        if not line.startswith(prefix):
            continue
        file, file2 = line[len(prefix) :].split(" ", 1)
        out.add(file)
        out.add(file2[2:])
    return tuple(out)


def is_media_only_patch(patch):
    files = get_modified_files(patch)
    return all(
        map(
            lambda x: is_media_file(x, MEDIA_PATHS + DOC_PATHS + DT_PATHS + MISC_PATHS),
            files,
        )
    )


def is_media_patch(patch):
    files = get_modified_files(patch)
    return any(
        map(
            lambda x: is_media_file(x, MEDIA_PATHS + DOC_PATHS + DT_PATHS + MISC_PATHS),
            files,
        )
    )


def is_doc_only_patch(patch):
    files = get_modified_files(patch)
    return all(map(lambda x: is_media_file(x, DOC_PATHS), files))


def is_dt_binding_only_patch(patch):
    files = get_modified_files(patch)
    return all(map(lambda x: is_media_file(x, DT_PATHS), files))


def is_misc_only_patch(patch):
    files = get_modified_files(patch)
    return all(map(lambda x: is_media_file(x, MISC_PATHS), files))


def is_dt_binding_patch(patch):
    files = get_modified_files(patch)
    return any(map(lambda x: is_media_file(x, DT_PATHS), files))
